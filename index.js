/* INITIALISATION */

var express = require('express');
var app = express();
var path = require('path');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var bodyParser = require('body-parser');
var session = require('express-session');
var cookieParser = require('cookie-parser');
var cors = require('cors');

mongoose.connect('mongodb://localhost:27017/taskycoin');

/* MIDDLEWARE */

var options = {
  dotfiles: 'ignore',
  etag: false,
  extensions: ['htm', 'html'],
  index: false,
  maxAge: '1d',
  redirect: false,
  setHeaders: function (res, path, stat) {
    res.set('x-timestamp', Date.now())
  }
}

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(cors());

app.use(cookieParser());
app.use(bodyParser());

app.use(session({
  key: 'user_sid',
  secret: 'putanginamo',
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 1200000
  }
}));

app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
      res.clearCookie('user_sid');
  }
  next();
});

/* ROUTES */

require('./config/routes.js')(app);

/* SERVER */

var port = 8169;

app.listen(port, function() {
	console.log('Listening on port ' + port);
});

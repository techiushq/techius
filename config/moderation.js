var mongoose = require('mongoose');
var request = require('request');
var sanitize = require('strip-js');
var User = require('../db/user.js');

exports.dashboard = function(req, res) {
	if(req.session.user.role == 2) {
		res.render('admin/dashboard', {
			title: 'Settings | Taskly',
			user: req.session.user
		});
	} else {
		res.redirect('/404');
	}
}

exports.quickBan = function(req, res) {
  if(req.session.user.role == 2) {
    var username = sanitize(req.body.username);
    if(username.length > 3 && username.length < 30) {
      var User = mongoose.model('User');
      User.update({'username': username}, {'role': 2}).exec();

      res.redirect('/admin');
    } else {
      res.redirect('/admin');
    }
  } else {
      res.redirext('/404');
  }
}

exports.quickRemove = function(req, res) {
  if(req.session.user.role == 2) {
    var listingID = sanitize(req.body.id);
    if(listingID.length == 24) {
      var Listing = mongoose.model('Listing');
      Listing.remove({'_id': listingID}).exec();

      res.redirect('/admin');
    } else {
      res.redirect('/admin');
    }
  } else {
      res.redirext('/404');
  }
}

exports.feeChange = function(req, res) {
  if(req.session.user.role == 2) {
    var amount = sanitize(req.body.amount);
    var amount = Number(amount);
    if(amount > 1.00 && amount < 1.20) {


      res.redirect('/admin');
    } else {
      res.redirect('/admin');
    }
  } else {
      res.redirext('/404');
  }
}

exports.menuSearch = function(req, res) {
	if(req.session.user.role == 2) {
		res.render('admin/menu', {
			title: 'Settings | ',
			user: req.session.user
		});
	} else {
		res.redirect('/404');
	}
}

exports.addCategory = function(req, res) {
  if(req.session.user.role == 2) {
    var addCategory = req.body.category;
    if(addCategory) {
      var Category = mongoose.model('Category');
      var newCategory = new Category();

      newCategory.category = addCategory;
      newCategory.created = Date.now();

      newCategory.save();

      res.send(200);
    } else {
      res.send(404);
    }
  }
}

exports.addSubCategory = function(req, res) {
  if(req.session.user.role == 2) {
    var addCategory = String(req.body.category).trim();
		var addSubCategory = String(req.body.subcategory).trim();

    if(addCategory && addSubCategory) {
      var subCategory = mongoose.model('subCategory');
			subCategory.findOne({"category": addCategory, "subCategory": addSubCategory}).exec(function(err, result) {
				if(err || result == '' || result == '{}' || result == '[]' || result == null) {
					var newSubCategory = new subCategory();

		      newSubCategory.category = addCategory;
		      newSubCategory.subCategory = addSubCategory;
		      newSubCategory.created = Date.now();

		      newSubCategory.save();

		      res.send(200);
				} else {
					res.send(403);
				}
			});
    } else {
      res.send(404);
    }
  } else {
    res.redirect('/404');
  }
}

exports.removeCategory = function(req, res) {
	if(req.session.user.role == 2) {
		var category = String(req.params.category).trim();

		if(category) {
			var Category = mongoose.model('subCategory');
			Category.remove({'category': category});

			res.send(200);
		} else {
			res.send(403);
		}
	} else {
		res.redirect('/404');
	}
}

exports.removeSubCategory = function(req, res) {
	if(req.session.user.role == 2) {
		var category = String(req.body.category).trim();
		var subCategory = String(req.body.subcategory).trim();

		if(category && subCategory) {
			var sub = mongoose.model('subCategory');
			sub.remove({'_id': subCategory});

			res.send(200);
		} else {
			res.send(403);
		}
	} else {
		res.redirect('/404');
	}
}

exports.addLocation = function(req, res) {

}

exports.removeLocation = function(req, res) {

}

exports.viewCategories = function(req, res) {
  if(req.session.user.role == 2) {
    var Category = mongoose.model('Category');
    Category.find({}).exec(function(err, result) {
      if(result) {
        res.send(result);
      } else {
        res.send('404');
      }

    });
  } else {
    res.redirect('/404');
  }
}

exports.viewSubCategories = function(req, res) {
  if(req.session.user.role == 2) {
    var subCategory = mongoose.model('subCategory');
    subCategory.find({}).exec(function(err, result) {
      if(result) {
        res.send(result);
      } else {
        res.send('404');
      }
    });
  } else {
    res.redirect('/404');
  }
}

exports.setAdmin = function(req, res) {
  var User = mongoose.model('User');
  User.count({'role': 2}).exec(function(err, userCount) {
    if(userCount == 0) {
      User.update({'username': req.session.user.username}, {'role': 2}).exec();
      res.redirect('/admin');
    } else {
      res.redirect('/404');
    }
  });
}

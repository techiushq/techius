var sanitize = require('strip-js');
var mongoose = require('mongoose');

exports.index = function(req, res) {
	res.render('help/index', {
		title: 'Help | Taskly',
		user: req.session.user
	});
};

// USER 

exports.userSettings = function(req, res) {
	res.render('help/', {
		title: 'How to edit settings | Taskly',
		user: req.session.user
	});
}

exports.userWallet = function(req, res) {
	res.render('help/', {
		title: 'How to manage wallet | Taskly',
		user: req.session.user
	});
}

exports.userAccount = function(req, res) {
	res.render('help/', {
		title: 'How to secure your account | ',
		user: req.session.user
	});
}

exports.userProfile = function(req, res) {
	res.render('help/', {
		title: 'Editing your profile | ',
		user: req.session.user
	});
}

// LISTING

exports.listingStart = function(req, res) {
	res.render('help/', {
		title: 'How to start a listing | ',
		user: req.session.user
	});
}

exports.listingEdit = function(req, res) {
	res.render('help/', {
		title: 'How to edit a listing | ',
		user: req.session.user
	});
}

exports.listingRemove = function(req, res) {
	res.render('help/', {
		title: 'How to remove a listing | ',
		user: req.session.user
	});
}

exports.listingFees = function(req, res) {
	res.render('help/', {
		title: 'Listing fees | ',
		user: req.session.user
	});
}

exports.listingType = function(req, res) {
	res.render('help/', {
		title: 'Can listing type be changed | ',
		user: req.session.user
	});
}

exports.listingReport = function(req, res) {
	res.render('help/', {
		title: 'How to report listings | ',
		user: req.session.user
	});
}

exports.listingManagement = function(req, res) {
	res.render('help/', {
		title: 'Listing management | ',
		user: req.session.user
	});
}

// DEAL

exports.dealFeedback = function(req, res) {
	res.render('help/', {
		title: 'How to give feedback | ',
		user: req.session.user
	});
}

exports.dealDispute = function(req, res) {
	res.render('help/', {
		title: 'How to start dispute | ',
		user: req.session.user
	});
}

exports.dealOffer = function(req, res) {
	res.render('help/', {
		title: 'How to revoke offer | ',
		user: req.session.user
	});
}

exports.dealDeal = function(req, res) {
	res.render('help/', {
		title: 'How to see deal | ',
		user: req.session.user
	});
}

exports.dealOptions = function(req, res) {
	res.render('help/', {
		title: 'Alternative payment options | ',
		user: req.session.user
	});
}

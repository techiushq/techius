var sanitize = require('strip-js');
var mongoose = require('mongoose');

exports.index = function(req, res) {
	res.render('general/index', {
		title: 'Home | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
};

exports.about = function(req, res) {
	res.render('general/about', {
		title: 'About | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.requests = function(req, res) {
	var page = Number(sanitize(req.query.p));
	if(page) {
		if(page >= 1 && page <= 20) {
			res.render('general/request', {
				title: 'Requests | Taskly',
				user: req.session.user,
				before: page - 1,
				page: page,
				next: page + 1,
				websiteName: 'Taskly'
			});
		} else {
			res.render('general/request', {
				title: 'Requests | Taskly',
				user: req.session.user,
				before: 1,
				page: 1,
				next: 2,
				websiteName: 'Taskly'
			});
		}
	} else {
		res.render('general/request', {
			title: 'Requests | Taskly',
			user: req.session.user,
			before: 1,
			page: 1,
			next: 2,
			websiteName: 'Taskly'	
		});
	}
}

exports.services = function(req, res) {
	var page = Number(sanitize(req.query.p));
	if(page) {
		if(page >= 1 && page <= 20) {
			res.render('general/services', {
				title: 'Services | Taskly',
				user: req.session.user,
				before: page - 1,
				page: page,
				next: page + 1,
				websiteName: 'Taskly'
			});
		} else {
			res.render('general/services', {
				title: 'Services | Taskly',
				user: req.session.user,
				before: 1,
				page: 1,
				next: 2,
				websiteName: 'Taskly'
			});
		}	
	} else {
		res.render('general/services', {
			title: 'Services | Taskly',
			user: req.session.user,
			before: 1,
			page: 1,
			next: 2,
			websiteName: 'Taskly'
		});
	}
}

exports.newListings = function(req, res) {
	var type = sanitize(req.params.type);
	if(type == 'services') {
		var Listing = mongoose.model('Listing');
		Listing.find({'type': 1, 'public': 1}).limit(3).sort({'created': -1}).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {
				res.send(result);
			}
		});
	} else {
		var Listing = mongoose.model('Listing');
		Listing.find({'type': 0, 'public': 1}).limit(3).sort({'created': -1}).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {
				res.send(result);
			}
		});
	}
}

exports.economy = function(req, res) {
	res.render('general/economy', {
		title: 'Economy | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.sitemap = function(req, res) {
	res.render('general/sitemap', {
		title: 'Sitemap | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.error = function(req, res) {
	res.render('general/error', {
		title: 'Error | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

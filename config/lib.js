var mongoose = require('mongoose');
var Transaction = require('../db/transaction');
var User = require('../db/user');

exports.removeNullsAndTrim = function(str) {
    if(typeof str === 'string')
        return str.replace(/\0/g, '').trim();
    else
        return str;
};

exports.newTransactionNoHolding = function(to, from, amount, reason) {
	var User = mongoose.model('User');
	User.findOne({'username': to}).select('_id username taskcoin').exec(function(err, toUserResult) {
		if(err || toUserResult == '' || toUserResult == '{}' || toUserResult == '[]' || toUserResult == null) {
			return false;
		} else {
			User.findOne({'username': from}).select('_id username taskcoin').exec(function(err, fromUserResult) {
				if(err || fromUserResult == '' || fromUserResult == '[]' || fromUserResult == '{}' || fromUserResult == null) {
					return false;
				} else {
					if(amount && reason) {
						// GIVE TO TO

						var newAmountTo = Number(toUserResult.taskcoin) + Number(amount);

						// DEDUCT FROM FROM

						var newAmountFrom = Number(fromUserResult.taskcoin) - Number(amount);

						// NEW TRANSACTION

						var Transaction = mongoose.model('Transaction');
						var newToTransaction = new Transaction();
						newToTransaction.from = fromUserResult.username;
						newToTransaction.to = toUserResult.username;
						newToTransaction.date = Date.now();
						newToTransaction.amount = 0 - Number(amount);
						newToTransaction.reason = reason;
						newToTransaction.save();

						var newFromTransaction = new Transaction();
						newFromTransaction.from = toUserResult.username;
						newFromTransaction.to = fromUserResult.username;
						newFromTransaction.date = Date.now();
						newFromTransaction.amount = 0 + Number(amount);
						newFromTransaction.reason = reason;
						newFromTransaction.save();

						// UPDATE TO & FROM

						User.update({'username': to}, {'taskcoin': newAmountTo}).exec();
						User.update({'username': from}, {'taskcoin': newAmountFrom}).exec();

						return true;
					} else {
						return false;
					}
				}
			});
		}
	});
}

exports.newHoldingTransaction = function(username, amount, reason) {
	var User = mongoose.model('User');
	User.findOne({'username': username}).select('_id username taskcoin holding').exec(function(err, userResult) {
		if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
			return false;
		} else {
			if(amount && reason) {
				
				// DEDUCT FROM TASKCOIN

				var newAmount = Number(userResult.taskcoin) - Number(amount);

				// INCREASE HOLDING

				var newHolding = Number(userResult.holding) + Number(amount);

				// NEW TRANSACTION

				var Transaction = mongoose.model('Transaction');
				var newTransaction = new Transaction();
				newTransaction.from = username;
				newTransaction.to = username;
				newTransaction.date = Date.now();
				newTransaction.amount = 0 - Number(amount);
				newTransaction.reason = reason;
				newTransaction.save();

				// UPDATE 

				User.update({'username': username}, {'taskcoin': newAmount, 'holding': newHolding}).exec();

				return true;
			} else {
				return false;
			}
		}
	});
}

exports.newTransactionHolding = function(to, from, amount, reason) {
	var User = mongoose.model('User');
	User.findOne({'username': to}).select('_id username taskcoin').exec(function(err, toUserResult) {
		if(err || toUserResult == '' || toUserResult == '[]' || toUserResult == '{}' || toUserResult == null) {
			return false;
		} else {
			User.findOne({'username': from}).select('_id username taskcoin holding').exec(function(err, fromUserResult) {
				if(err || fromUserResult == '' || fromUserResult == '[]' || fromUserResult == '{}' || fromUserResult == null) {
					return false;
				} else {
					if(amount && reason) {

						// DEDUCT FROM FROM HOLDING

						var newHolding = Number(fromUserResult.holding) - Number(amount);

						// INCREASE TO TASKCOIN

						var newAmount = Number(toUserResult.taskcoin) + Number(amount);

						// NEW TRANSACTIONS

						var Transaction = mongoose.model('Transaction');
						var newToTransaction = new Transaction();
						newToTransaction.from = to;
						newToTransaction.to = from;
						newToTransaction.date = Date.now();
						newToTransaction.amount = 0 + Number(amount);
						newToTransaction.reason = reason;
						newToTransaction.save();

						var newFromTransaction = new Transaction();
						newFromTransaction.from = from;
						newFromTransaction.to = to;
						newFromTransaction.date = Date.now();
						newFromTransaction.amount = 0 - Number(amount);
						newFromTransaction.reason = reason;
						newToTransaction.save();

						// UPDATE

						User.update({'username': from}, {'holding': newHolding}).exec();
						User.update({'username': to}	, {'amount': newAmount}).exec();

						return true;
					} else {
						return false;
					}
				}
			});
		}
	});
};
var mongoose = require('mongoose');
var User = require('../db/user');
var Listing = require('../db/listing');
var Category = require('../db/category');
var subCategory = require('../db/subcategory');
var location = require('../db/location');
var Dispute = require('../db/dispute');
var disputeVote = require('../db/disputevote');
var disputeMessage = require('../db/disputemessages');
var Deal = require('../db/deal');
var sanitize = require('strip-js');
var bcrypt = require('bcrypt-nodejs');

// VIEWS

exports.search = function(req, res) {
  res.render('search', {
    title: 'Search | Taskly',
    user: req.session.user,
    reason: '',
    websiteName: 'Taskly'
  });
}

exports.result = function(req, res) {
  var keywords = String(req.query.keywords).trim();
  var type = String(req.query.type).trim();
  var category = String(req.query.category).trim();
  var subcategory = String(req.query.subcategory).trim();
  var location = String(req.query.location).trim();

  var pageNumber = Number(sanitize(req.query.p));

  function errRender(reason) {
    res.render('search', {
      title: 'Search | Taskly',
      user: req.session.user,
      reason: reason,
      websiteName: 'Taskly'
    });
  } 

  if(type && category && subcategory && location) {
    if(type == '0' || type == '1') {
      var Category = mongoose.model('Category');
      Category.findOne({'category': category}).exec(function(err, categoryResult) {
        if(err || categoryResult == '' || categoryResult == '[]' || categoryResult == '{}' || categoryResult == null) {
          errRender('Select a category');
        } else {
          var subCategory = mongoose.model('subCategory');
          subCategory.findOne({'category': category, 'subCategory': subcategory}).exec(function(err, subCategoryResult) {
            if(err || subCategoryResult == '[]' || subCategoryResult == '{}' || subCategoryResult == '' || subCategoryResult == null) {
              errRender('Select a subcategory');
            } else {
              if(location == 'International') {
                if(pageNumber) {
                  if(pageNumber >= 1 && pageNumber <= 20) {
                    res.render('result', {
                      title: 'Search | Taskly',
                      user: req.session.user,
                      keywords: keywords,
                      type: type,
                      category: category,
                      subcategory: subcategory,
                      location: location,
                      before: pageNumber - 1,
                      page: pageNumber,
                      next: pageNumber + 1,
                      websiteName: 'Taskly'
                    });
                  } else {
                    res.render('result', {
                      title: 'Search | Taskly',
                      user: req.session.user,
                      keywords: keywords,
                      type: type,
                      category: category,
                      subcategory: subcategory,
                      location: location,
                      before: 1,
                      page: 1,
                      next: 2,
                      websiteName: 'Taskly'
                    });
                  }
                } else {
                  res.render('result', {
                    title: 'Search | Taskly',
                    user: req.session.user,
                    keywords: keywords,
                    type: type,
                    category: category,
                    subcategory: subcategory,
                    location: location,
                    before: 1,
                    page: 1,
                    next: 2,
                    websiteName: 'Taskly'
                  });
                }
              } else {
                errRender('Select a location');
              }
            } 
          });
        } 
      });
    } else {
      errRender('Select a listing type');
    }
  } else {
    res.redirect('/404');
  }
}

// API

exports.query = function(req, res) {
  var keywords = String(req.query.keywords).trim();
  var type = String(req.query.type).trim();
  var category = String(req.query.category).trim();
  var subcategory = String(req.query.subcategory).trim();
  var location = String(req.query.location).trim();
  var time = String(req.query.time).trim();

  var pageNumber = Number(sanitize(req.query.p));

  function errRender(reason) {
    res.render('search', {
      title: 'Search | Taskly',
      user: req.session.user,
      reason: reason,
      websiteName: 'Taskly'
    });
  } 

  if(type && category && subcategory && location) {    
    if(type == '0' || type == '1') {
      var Category = mongoose.model('Category');
      Category.findOne({'category': category}).exec(function(err, categoryResult) {
        if(err || categoryResult == '' || categoryResult == '[]' || categoryResult == '{}' || categoryResult == null) {
          errRender('Select a category');
        } else {
          var subCategory = mongoose.model('subCategory');
          subCategory.findOne({'category': category, 'subCategory': subcategory}).exec(function(err, subCategoryResult) {
            if(err || subCategoryResult == '[]' || subCategoryResult == '{}' || subCategoryResult == '' || subCategoryResult == null) {
              errRender('Select a subcategory');
            } else {
              if(location == 'International') {
                if(pageNumber) {
                  if(pageNumber >= 1 && pageNumber <= 20) {

                    var Listing = mongoose.model('Listing');
                    Listing.paginate({'title': new RegExp(keywords), 'type': type, 'location': location, 'category': category, 'subcategory': subcategory, 'public': 1}, {page: pageNumber, limit: 12}, function(err, result) {
                      if(err || result == '' || result == '[]' || result == '{}' || result == null) {
                        res.send(String('0'));
                      } else {
                        res.send(result.docs);
                      }
                    });
                  } else {
                    var Listing = mongoose.model('Listing');
                    Listing.find({'title': new RegExp(keywords), 'type': type, 'location': location, 'category': category, 'subcategory': subcategory, 'public': 1}).limit(12).exec(function(err, result) {
                      if(err || result == '' || result == '[]' || result == '{}' || result == null) {
                        res.send(String('0'));
                      } else {
                        res.send(result);
                      }
                    });
                  }
                } else {
                  var Listing = mongoose.model('Listing');
                  Listing.find({'title': new RegExp(keywords), 'type': type, 'location': location, 'category': category, 'subcategory': subcategory, 'public': 1}).limit(12).exec(function(err, result) {
                    if(err || result == '' || result == '[]' || result == '{}' || result == null) {
                      res.send(String('0'));
                    } else {
                      res.send(result);
                    }
                  });
                }
              } else {
                errRender('Select a location');
              }
            }
          }); 
        }   
      });
    } else {
      errRender('Select a listing type');
    }
  } else {
    res.redirect('/404');
  }  
}

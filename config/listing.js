var mongoose = require('mongoose');
var User = require('../db/user');
var Listing = require('../db/listing');
var Category = require('../db/category');
var subCategory = require('../db/subcategory');
var location = require('../db/location');
var Offer = require('../db/offer');
var Transaction = require('../db/transaction');
var Feedback = require('../db/feedback');
var Deal = require('../db/deal');
var Dispute = require('../db/dispute');
var Watchlist = require('../db/watchlist');
var sanitize = require('strip-js');
var lib = require('../config/lib.js');

// LISTING

exports.createService = function(req, res) {
	var titleForm = String(req.body.title).trim();
	var categoryForm = String(sanitize(req.body.category)).trim();
	var subCategoryForm = sanitize(String(req.body.subcategory)).trim();
	var descriptionForm = String(req.body.description).trim();
	var locationForm = sanitize(String(req.body.location)).trim();
	var doneForm = sanitize(String(req.body.done)).trim();
	var amountForm = sanitize(String(req.body.amount)).trim();
	var imageForm = String(req.body.Image).trim();

	if(titleForm && categoryForm && subCategoryForm && descriptionForm && locationForm && doneForm && amountForm) {
		if(titleForm.length >= 5 && titleForm.length <= 80) {
			var Category = mongoose.model('Category');
			Category.findOne({'category': categoryForm}).exec(function(err, categoryResult) {
				if(err || categoryResult == '' || categoryResult == '[]' || categoryResult == '{}' || categoryResult == null) {
					res.render('listinginfo', {
						title: 'New Service Listing | ',
						user: req.session.user,
						type: 'service',
						reason: 'Select a category',
						websiteName: 'Taskly'
					});
				} else {
					var subCategory = mongoose.model('subCategory');
					subCategory.findOne({'category': categoryForm, 'subCategory': subCategoryForm}).exec(function(err, subCategoryResult) {
						if(err || subCategoryResult == '' || subCategoryResult == '{}' || subCategoryResult == '[]' || subCategoryResult == null) {
							res.render('listinginfo', {
								title: 'New Service Listing | Taskly',
								user: req.session.user,
								type: 'service',
								reason: 'Select a sub-category',
								websiteName: 'Taskly'
							});
						} else {
							if(descriptionForm.length >= 30 && descriptionForm.length <= 10000) {
								if(locationForm != 'International') {
									res.render('listinginfo', {
										title: 'New Service Listing | Taskly',
										user: req.session.user,
										type: 'service',
										reason: 'Select a location',
										websiteName: 'Taskly'
									});
								} else {
									if(doneForm == '24' || doneForm == '3' || doneForm == '1') {
										var User = mongoose.model('User');
										User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
											if(err || userResult == '' || userResult == '{}' || userResult == '[]' || userResult == null) {
												res.redirect('/sign-up');
											} else {

												// TAX 

												var tax = Number(amountForm) * 0.01;

												if(tax >= Number(userResult.taskcoin)) {
													res.render('listinginfo', {
														title: 'New Service Listing | Taskly',
														user: req.session.user,
														type: 'service',
														reason: 'Not enough Taskly to pay for fee',
														websiteName: 'Taskly'
													});
												} else {

													// CREATE TRANSACTION

													lib.newTransactionNoHolding('taskcoin', req.session.user.username, tax, 'New listing created. Listing \'tax\' paid.');

													// CREATE LISTING

													var Listing = mongoose.model('Listing');
													var newListing = new Listing();

													newListing.username = req.session.user.username;
													newListing.created = Date.now();
													newListing.category = categoryForm;
													newListing.subcategory = subCategoryForm;
													newListing.location = locationForm;
													newListing.title = titleForm;
													newListing.description = descriptionForm
													newListing.amount = amountForm;
													newListing.type = 1;
													newListing.feedback = 0;
													newListing.edited = 0;
													newListing.public = 1;
													newListing.done = doneForm;
													newListing.photo = imageForm;

													newListing.save(function(err, save) {
														res.redirect('/s/'+save._id);
													});
												}
											}
										});
									} else {
										res.render('listinginfo', {
											title: 'New Service Listing | Taskly',
											user: req.session.user,
											type: 'service',
											reason: 'Select a completion time',
											websiteName: 'Taskly'
										});
									}
								}
							} else {
								res.render('listinginfo', {
									title: 'New Service Listing | Taskly',
									user: req.session.user,
									type: 'service',
									reason: 'Description must be greater than 30 and less than 10,000 characters',
									websiteName: 'Taskly'
								});
							}
						}
					});
				}
			});
		} else {
			res.render('listinginfo', {
				title: 'New Service Listing | Taskly',
				user: req.session.user,
				type: 'service',
				reason: 'Title length must be greater than 5 and less than 80 characters',
				websiteName: 'Taskly'
			});
		}
	} else {
		res.render('listinginfo', {
			title: 'New Service Listing | Taskly',
			user: req.session.user,
			type: 'service',
			reason: 'Fill in all required forms',
			websiteName: 'Taskly'
		});
	}

	
}

exports.createRequest = function(req, res) {
	var titleForm = String(req.body.title).trim();
	var categoryForm = sanitize(String(req.body.category)).trim();
	var subCategoryForm = sanitize(String(req.body.subcategory)).trim();
	var descriptionForm = String(req.body.description).trim();
	var locationForm = sanitize(String(req.body.location)).trim();
	var doneForm = sanitize(String(req.body.done)).trim();
	var amountForm = sanitize(String(req.body.amount)).trim();
	var imageForm = String(req.body.image).trim();

	// CHECK THAT PRICE IS A NUMBER

	if(titleForm.length >= 5 && titleForm.length <= 80) {
		var Category = mongoose.model('Category');
		Category.findOne({'category': categoryForm}).exec(function(err, categoryResult) {
			if(err || categoryResult == '' || categoryResult == '[]' || categoryResult == '{}' || categoryResult == null) {
				res.render('listinginfo', {
					title: 'New Request Listing | Taskly',
					user: req.session.user,
					type: 'request',
					reason: 'Select a category',
					websiteName: 'Taskly'
				});
			} else {
				var subCategory = mongoose.model('subCategory');
				subCategory.findOne({'category': categoryForm, 'subCategory': subCategoryForm}).exec(function(err, subCategoryResult) {
					if(err || subCategoryResult == '' || subCategoryResult == '{}' || subCategoryResult == '[]' || subCategoryResult == null) {
						res.render('listinginfo', {
							title: 'New Request Listing | Taskly',
							user: req.session.user,
							type: 'request',
							reason: 'Select a sub-category',
							websiteName: 'Taskly'
						});
					} else {
						if(descriptionForm.length >= 30 && descriptionForm.length <= 10000) {
							if(locationForm != 'International') {
								res.render('listinginfo', {
									title: 'New Request Listing | Taskly',
									user: req.session.user,
									type: 'request',
									reason: 'Select a location',
									websiteName: 'Taskly'
								});
							} else {
								if(doneForm == '24' || doneForm == '3' || doneForm == '1') {
									var User = mongoose.model('User');
									User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
										if(err || userResult == '' || userResult == '{}' || userResult == '[]' || userResult == null) {
											res.redirect('/sign-up');
										} else {
											var totalDue = amountForm * 1.01;

											if(totalDue >= Number(userResult.taskcoin)) {
												res.render('listinginfo', {
													title: 'New Request Listing | Taskly',
													user: req.session.user,
													type: 'request',
													reason: 'Not enough Taskly to pay for fee',
													websiteName: 'Taskly'
												});
											} else {

												// PAY TAX

												var tax = amountForm * 0.01;

												lib.newTransactionNoHolding('taskcoin', req.session.user.username, tax, 'New listing created. Listing \'tax\' paid.');

												// DEDUCT HOLDING

												lib.newHoldingTransaction(req.session.user.username, amountForm, 'New request listing created. Amount being transferred from main wallet to holding.');

												var Listing = mongoose.model('Listing');
												var newListing = new Listing();

												newListing.username = req.session.user.username;
												newListing.created = Date.now();
												newListing.category = categoryForm;
												newListing.subcategory = subCategoryForm;
												newListing.location = locationForm;
												newListing.title = titleForm;
												newListing.description = descriptionForm
												newListing.amount = amountForm;
												newListing.type = 0;
												newListing.feedback = 0;
												newListing.edited = 0;
												newListing.public = 1;
												newListing.done = doneForm;
												newListing.photo = imageForm;

												newListing.save(function(err, save) {
													res.redirect('/r/'+save._id);
												});
											}
										}
									});
								} else {
									res.render('listinginfo', {
										title: 'New Request Listing | Taskly',
										user: req.session.user,
										type: 'request',
										reason: 'Select a completion time',
										websiteName: 'Taskly'
									});
								}
							}
						} else {
							res.render('listinginfo', {
								title: 'New Request Listing | Taskly',
								user: req.session.user,
								type: 'request',
								reason: 'Description must be greater than 30 and less than 10,000 characters',
								websiteName: 'Taskly'
							});
						}
					}
				});
			}
		});
	} else {
		res.render('listinginfo', {
			title: 'New Request Listing | Taskly',
			user: req.session.user,
			type: 'request',
			reason: 'Title length must be greater than 5 and less than 80 characters',
			websiteName: 'Taskly'
		});
	}
}

exports.edit = function(req, res) {
	var id = sanitize(req.params.id);
	var Listing = mongoose.model('Listing');
	Listing.findOne({'_id': id, 'username': req.session.user.username, 'public': true}).exec(function(err, listingResult) {
		if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
			res.redirect('/404');
		} else {	
			res.render('listing/edit', {
				title: 'Edit | Taskly',
				user: req.session.user,
				id: String(id),
				reason: '',
				type: listingResult.type,
				title: listingResult.title,
				price: listingResult.amount,
				description: listingResult.description,
				photo: listingResult.photo,
				category: listingResult.category,
				subcategory: listingResult.subcategory,
				websiteName: 'Taskly'
			});
		}
	});
}

exports.postEdit = function(req, res) {
	var id = sanitize(String(req.params.id)).trim();
	var title = sanitize(String(req.body.title)).trim();
	var price = Number(sanitize(req.body.amount)).trim();
	var description = String(req.body.description).trim();
	var photo = String(req.body.photo).trim();
	var category = sanitize(String(req.body.category)).trim();
	var subCat = sanitize(String(req.body.subcategory)).trim();

	function errRender(id, reason) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': id}).exec(function(err, result) {
			res.render('listing/edit', {
				title: 'Edit | Taskly',
				user: req.session.user.username,
				id: String(id),
				reason: reason,
				type: result.type,
				title: result.title,
				price: result.amount,
				description: result.description,
				photo: result.photo,
				category: result.category,
				subcategory: result.subcategory,
				websiteName: 'Taskly'
			});
		});
	}

	if(title && price && description && category && subCat) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': id, 'username': req.session.user.username, 'public': true}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.redirect('/404');
			} else {	
				if(title.length >= 5 && title.length <= 80) {
					if(description.length >= 30 && description.length <= 10000) {
						var subCategory = mongoose.model('subCategory');
						subCategory.findOne({'category': category, 'subCategory': subCat}).exec(function(err, result) {
							if(err || result == '' || result == '[]' || result == '{}' || result == null) {
								errRender(id, 'Check your category and/or sub-category');
							} else {
								if(listingResult.type == true) {
									// SERVICE

									if(price == Number(listingResult.amount)) {

										Listing.update({'_id': id}, {'title': title, 'description': description, 'photo': photo, 'category': category, 'subCategory': subCat, 'edited': 1}).exec();

										res.redirect('/s/'+id);

									} else {
										
										// CHARGE NEW FEE

										if(Number(price) >= 100) {
											var User = mongoose.model('User');
											User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
												if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
													res.redirect('/404');
												} else {

													var total = price * 0.01;

													if(Number(userResult.taskcoin) >= total) {	

														lib.newTransactionNoHolding('taskcoin', req.session.user.username, total, 'Updated listing. New \'tax\' applied.');

														// UPDATE LISTING

														Listing.update({'_id': id}, {'title': title, 'description': description, 'amount': Number(price), 'photo': photo, 'category': category, 'subCategory': subCat}).exec();

														res.redirect('/s/'+id);

													} else {
														errRender(id, 'Not enough Taskly to pay for fee');
													}
												}
											});
										} else {
											errRender(id, 'Amount must be greater than 100 Taskly');
										}
									}

								} else {
									// REQUEST

									if(price == Number(listingResult.amount)) {
										// UPDATE LISTING

										Listing.update({'_id': id}, {'title': title, 'description': description, 'photo': photo, 'category': category, 'subCategory': subCategory, 'edited': 1}).exec();

										res.redirect('/r/'+id);

									} else {

										if(price >= 100) {
											var User = mongoose.model('User');
											User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
												if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
													res.redirect('/404');
												} else {
													var total = price * 1.01;
													if(Number(userResult.taskcoin) >= total) {	

														// REMOVE HOLDING, MAKE THIS A TRANSACTION!

														var Transaction = mongoose.model('Transaction');
														var newTransaction = new Transaction();
														newTransaction.from = '';
														newTransaction.to = req.session.user.username;
														newTransaction.date = Date.now();
														newTransaction.amount = Number(listingResult.amount);
														newTransaction.reason = 'Removing funds from "holding"';
														newTransaction.save();

														var newHolding = Number(userResult.holding) - Number(listingResult.amount);
														var newTaskly = Number(userResult.taskcoin) + Number(listingResult.amount);

														User.update({'username': req.session.user.username}, {'taskcoin': newTaskly, 'holding': newHolding}).exec();

														// TAX

														lib.newTransactionNoHolding('taskcoin', req.session.user.username, total, 'Updated listing. New listing fee applied.');

														// UPDATE LISTING

														Listing.update({'_id': id}, {'title': title, 'description': description, 'amount': Number(price), 'photo': photo, 'category': category, 'subCategory': subCategory}).exec();

														res.redirect('/s/'+id);

													} else {
														errRender(id, 'Not enough Taskly to pay for fee');
													}
												}
											});
										} else {
											errRender(id, 'Amount must be greater than 100 Taskly');
										}
									}
								}
							}
						});
					} else {
						errRender(id, 'Description must be greater than 30 and less than 10,000 characters');
					}
				} else {
					errRender(id, 'Title length must be greater than 5 and less than 80 characters');
				}
			}
		});
	} else {
		errRender(id, 'Ensure that all required forms are filled out')
	}	
}

exports.removeListing = function(req, res) {
	var id = sanitize(String(req.params.id)).trim();
	if(id) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': id, 'username': req.session.user.username, 'public': true}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.redirect('/404');
			} else {

				// CHECK IF THERE IS AN OFFER, DEAL OR DISPUTE 

				var Offer = mongoose.model('Offer');
				Offer.findOne({'listingID': id}).exec(function(err, result) {
					if(result) {
						res.send('Delete all offers before proceeding');
					} else {
						var Deal = mongoose.model('Deal');
						Deal.find({'listingID': id}).exec(function(err, result) {
							if(result) {
								Listing.update({'_id': id}, {'public': false, 'offline': Date.now()}).exec();

								res.send(200);
							} else {
								Listing.findOneAndRemove({'_id': id}).exec();

								res.send(200);
							}
						});
					}
				});

			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.category = function(req, res) {
	var Category = mongoose.model('Category');
	Category.find({}).exec(function(err, result) {
		if(err) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.subCategory = function(req, res) {
	var category = sanitize(req.params.category);
	if(category) {
		var subCategory = mongoose.model('subCategory');
		subCategory.find({'category': category}).exec(function(err, result) {
			res.send(result);
		});
	} else {
		res.send(404);
	}
}

/* WEBSITE */

exports.newListing = function(req, res) {
	res.render('newlisting', {
		title: 'New Listing | Taskly',
		user: req.session.user,
    	websiteName: 'Taskly'
	});
}

exports.serviceNew = function(req, res) {
	res.render('listinginfo', {
		title: 'New Service Listing | Taskly',
		user: req.session.user,
		type: 'service',
		reason: '',
    	websiteName: 'Taskly'
	});
}

exports.requestNew = function(req, res) {
	res.render('listinginfo', {
		title: 'New Request Listing | Taskly',
		user: req.session.user,
		type: 'request',
		reason: '',
    	websiteName: 'Taskly'
	});
}

exports.r = function(req, res) {
	var id = sanitize(req.params.id);
	if(id) {
		if(id.length == 24) {
			var Listing = mongoose.model('Listing');
			Listing.findOne({'_id': id, 'type': 0}).exec(function(err, listingResult) {
				if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
					res.redirect('/404');
				} else {
					var User = mongoose.model('User');
					User.findOne({'username': listingResult.username}).select('_id username biography created').exec(function(err, userResult) {
						if(err || userResult == '' || userResult == '{}' || userResult == '[]' || userResult == null) {
							res.redirect('/404');	
						} else {
							res.render('r', {
								title: listingResult.title + ' | Taskly',
								user: req.session.user,
								listingID: String(id),
								listingTitle: listingResult.title,
								listingAmount: listingResult.amount,
								listingDescription: listingResult.description,
								listingCategory: listingResult.category,
								listingSubCategory: listingResult.subcategory,
								listingLocation: listingResult.location,
								listingType: listingResult.type,
								listingDone: listingResult.done,
								opJoinDate: String(userResult.created).slice(4,15),
								opBio: String(userResult.biography),
								public: String(listingResult.public),
								op: String(listingResult.username),
								websiteName: 'Taskly'
							});
						}
					});
				}
			});
		} else {
			res.redirect('/404');
		}
	} else {
		res.redirect('/404');	
	}
}

exports.s = function(req, res) {
	var id = sanitize(req.params.id);
	if(id) {
		if(id.length == 24) {
			var Listing = mongoose.model('Listing');
			Listing.findOne({'_id': id, 'type': 1}).exec(function(err, listingResult) {
				if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
					res.redirect('/404');
				} else {
					var User = mongoose.model('User');
					User.findOne({'username': listingResult.username}).select('_id username biography created').exec(function(err, userResult) {
						if(err || userResult == '' || userResult == '{}' || userResult == '[]' || userResult == null) {
							res.redirect('/404');	
						} else {	
							res.render('s', {
								title: listingResult.title + ' | Taskly',
								user: req.session.user,
								listingID: String(id),
								listingTitle: listingResult.title,
								listingAmount: listingResult.amount,
								listingDescription: listingResult.description,
								listingCategory: listingResult.category,
								listingSubCategory: listingResult.subcategory,
								listingLocation: listingResult.location,
								listingType: listingResult.type,
								listingDone: listingResult.done,
								opJoinDate: String(userResult.created).slice(4,15),
								opBio: String(userResult.biography),
								public: String(listingResult.public),
								op: String(listingResult.username),
								websiteName: 'Taskly'
							});
						}
					});
				}
			});
		} else {
			res.redirect('/404');
		}
	} else {
		res.redirect('/404');
	}
}

exports.purchase = function(req, res) {
	var id = sanitize(req.params.id);
	if(id) {
		if(id.length == 24) {
			var Listing = mongoose.model('Listing');
			Listing.findOne({'_id': id}).exec(function(err, listingResult) {
				if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
					res.redirect('/404');
				} else {
					var User = mongoose.model('User');
					User.findOne({'username': listingResult.username}).select('_id username biography created').exec(function(err, userResult) {
						if(err || userResult == '' || userResult == '{}' || userResult == '[]' || userResult == null) {
							res.redirect('/404');	
						} else {	
							res.render('purchase', {
								title: 'Make Offer | Taskly',
								user: req.session.user,
								reason: '',
								listingID: id,
								listingTitle: listingResult.title,
								listingAmount: listingResult.amount,
								listingDescription: listingResult.description.split(0,100),
								listingCategory: listingResult.category,
								listingSubCategory: listingResult.subcategory,
								listingLocation: listingResult.location,
								listingType: listingResult.type,
								listingDone: listingResult.done,
								opJoinDate: String(userResult.created).slice(4,15),
								opBio: String(userResult.biography),
								op: String(listingResult.username),
								websiteName: 'Taskly'
							});
						}
					});
				}
			});
		} else {
			res.redirect('/404');
		}
	} else {
		res.redirect('/404');
	}
}

exports.apiListing = function(req, res) {
	var id = sanitize(req.params.listingID);
	if(id) {
		if(id.length == 24) {
			var Listing = mongoose.model('Listing');
			Listing.findOne({'_id': id}).exec(function(err, listingResult) {
				if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
					res.send(404);
				} else {
					res.send(listingResult);
				}
			});
		} else {
			res.send(404);
		}
	} else {
		res.redirect('/404');
	}
}

exports.viewFeedback = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Feedback = mongoose.model('Feedback');
		Feedback.find({'listingID': listingID}).limit(3).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {
				res.send(result);
			}
		});
	} else {
		res.redirect('/404');
	}
}	

exports.feedbackCount = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Feedback = mongoose.model('Feedback');
		Feedback.count({'listingID': listingID}).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {
				res.send(result);
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.userInfo = function(req, res) {
	var username = sanitize(req.params.username);
	if(username) {
		var User = mongoose.model('User');
		User.findOne({'username': username}).select('_id username biography created').exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(404);
			} else {
				res.send(result);
			}
		});
	} else {
		res.send(404);
	}
}

exports.watchlistAdd = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': listingID, 'public': 1, 'type': 1}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.redirect('/404');
			} else {
				if(req.session.user.username == listingResult.username) {
					res.redirect('/404');
				} else {
					var Watchlist = mongoose.model('Watchlist');
					Watchlist.findOne({'username': req.session.user.username, 'listingID': listingID}).exec(function(err, watchlistResult) {
						if(err || watchlistResult == '' || watchlistResult == '[]' || watchlistResult == '{}' || watchlistResult == null) {
							var newWatchlist = new Watchlist();
							newWatchlist.username = req.session.user.username;
							newWatchlist.listingID = listingID;
							newWatchlist.date = Date.now();
							newWatchlist.save();

							res.redirect('/user/requests/watchlist');
						} else {
							res.redirect('/user/requests/watchlist');
						}
					});
				}
			}
		});		
	} else {
		res.redirect('/404');
	}
}

exports.servicesAPI = function(req, res) {
	var pageNumber = Number(sanitize(req.query.p));

	var Listing = mongoose.model('Listing');

	if(pageNumber) {
		if(pageNumber >= 1 && pageNumber <= 20) {
			Listing.paginate({'type': 1, 'public': true}, {page: pageNumber, limit: 12}, function(err, listingResult) {
				if(err) {
					res.send(404);
				} else {
					res.send(listingResult.docs);
				}
			});
		} else {
			Listing.find({'type': 1, 'public': true}).limit(12).exec(function(err, listingResult) {
				if(err) {
					res.send(404);
				} else {
					res.send(listingResult);
				}
			});
		}
	} else {
		Listing.find({'type': 1, 'public': true}).limit(12).exec(function(err, listingResult) {
			if(err) {
				res.send(404);
			} else {
				res.send(listingResult);
			}
		});
	}
}

exports.requestsAPI = function(req, res) {
	var pageNumber = Number(sanitize(req.query.p));

	var Listing = mongoose.model('Listing');

	if(pageNumber) {
		if(pageNumber >= 1 && pageNumber <= 20) {
			Listing.paginate({'type': 0, 'public': true}, {page: pageNumber, limit: 12}, function(err, listingResult) {
				if(err) {
					res.send(404);
				} else {
					res.send(listingResult.docs);
				}
			});	
		} else {
			Listing.find({'type': 0, 'public': true}).limit(12).exec(function(err, listingResult) {
				if(err) {
					res.send(404);
				} else {
					res.send(listingResult);
				}
			});
		}
	} else {
		Listing.find({'type': 0, 'public': true}).limit(12).exec(function(err, listingResult) {
			if(err) {
				res.send(404);
			} else {
				res.send(listingResult);
			}
		});
	}
}

exports.feedback = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': listingID}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.redirect('/404');
			} else {
				res.render('listing/feedback', {
					title: 'Feedback | Taskly',
					user: req.session.user
				});
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.feedbackAPI = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': listingID}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.redirect('/404');
			} else {
				var Feedback = mongoose.model('Feedback');
				Feedback.find({'listingID': listingID}).exec(function(err, result) {
					if(err || result == '' || result == '[]' || result == '{}' || result == null) {
						res.send(String('0'));
					} else {
						res.send(result);
					}
				});
			}
		});
	} else {
		res.redirect('/404');
	}
}	


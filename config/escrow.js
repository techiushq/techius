var mongoose = require('mongoose');
var User = require('../db/user');
var Listing = require('../db/listing');
var Category = require('../db/category');
var subCategory = require('../db/subcategory');
var location = require('../db/location');
var Dispute = require('../db/dispute');
var disputeVote = require('../db/disputevote');
var disputeMessage = require('../db/disputemessages');
var Deal = require('../db/deal');
var sanitize = require('strip-js');
var bcrypt = require('bcrypt-nodejs');

// DISPUTES

exports.voteDispute = function(req, res) {
  var disputeID = sanitize(String(req.params.disputeID)).trim();
  var vote = sanitize(String(req.body.vote)).trim();

  if(disputeID && vote) {
    if(disputeID.length == 24) {
      var Dispute = mongoose.model('Dispute');
      Dispute.findOne({'_id': disputeID}).exec(function(err, disputeResult) {
        if(err || disputeResult == '' || disputeResult == '[]' || disputeResult == '{}' || disputeResult == null) {
          res.redirect('/404');
        } else {
          var disputeVote = mongoose.model('disputeVote');
          disputeVote.findOne({'disputeID': disputeID, 'username': req.session.user.username}).exec(function(err, disputeVoteResult) {
            if(err || disputeVoteResult == '' || disputeVoteResult == '[]' || disputeVoteResult == '{}' || disputeVoteResult == null) {
              if(disputeResult.worker == req.session.user.username || disputeResult.customer == req.session.user.username) {
                res.redirect('/dispute/'+disputeResult._id);
              } else {
                if(Number(disputeResult.verdict) == 0) {
                  var Deal = mongoose.model('Deal');
                  Deal.findOne({'_id': disputeResult.dealID}).exec(function(err, dealResult) {
                    if (err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
                      res.redirect('/404');
                    } else {
                      if(vote == disputeResult.worker || vote == disputeResult.customer) {
                        var disputeVote = mongoose.model('disputeVote');
                        disputeVote.count({'disputeID': disputeID}).exec(function(err, count) {
                          if(err) {
                            return 0;
                          } else {
                            if(count >= 0 && count <= 17) {
                              // CAST VOTE

                              var User = mongoose.model('User');
                              User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
                                if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
                                  res.redirect('/sign-in');
                                } else {
                                  if(Number(userResult.taskcoin <= 500)) {
                                    res.send(String('Not enough TaskCoin'));
                                  } else {
                                    var newAmount = Number(userResult.taskcoin) - 500;
                                    var newHolding = Number(userResult.holding) + 500;
                                    User.update({'username': req.session.user.username}, {'taskcoin': newAmount, 'holding': newHolding}).exec();

                                    // SHOULD CREATE TRANSACTION TOO

                                    disputeVote.findOne({'username': req.session.user.username, 'disputeID': disputeID}).exec(function(err, disputeVoteResult) {
                                      if(err || disputeVoteResult == '' || disputeVoteResult == '' || disputeVoteResult == '' || disputeVoteResult == null) {
                                        var newDisputeVote = new disputeVote();
                                        newDisputeVote.username = req.session.user.username;
                                        newDisputeVote.disputeID = disputeID;
                                        newDisputeVote.date = Date.now();
                                        newDisputeVote.side = vote;
                                        newDisputeVote.amount = 500;

                                        newDisputeVote.save();
                                      } else {
                                        return 0;
                                      }
                                    });

                                    var Transaction = mongoose.model('Transaction');
                                    var newTransaction = new Transaction();
                                    newTransaction.from = '';
                                    newTransaction.to = req.session.user.username;
                                    newTransaction.date = Date.now();
                                    newTransaction.amount = dealResult.amount;
                                    newTransaction.reason = "Voted on dispute. T500 transferred from main wallet to holding";
                                    newTransaction.save();

                                    res.redirect('/dispute/'+disputeResult._id);
                                  }
                                }
                              });
                            } else {
                              // WINNER

                              res.send('Can\'t vote anymore');
                            }
                          }
                        });
                      } else {
                        res.send(String('Not enough TaskCoin'));  
                      }
                    }
                  }); 
                } else {
                  res.send('Verdict already decided');
                }
              }
            } else {
              res.send('Already voted');
            }
          });
        }
      });
    } else {
      res.redirect('/404');
    }
  } else {
    res.redirect('/404');
  }
}

exports.apiDispute = function(req, res) {
  var disputeID = sanitize(req.params.disputeID);
  if(disputeID) {
    var Dispute = mongoose.model('Dispute');
    Dispute.findOne({'_id': disputeID}).exec(function(err, disputeResult) {
      if(err || disputeResult == '' || disputeResult == '[]' || disputeResult == '{}' || disputeResult == null) {
        res.send(404);
      } else {
        res.send(disputeResult);
      }
    });
  } else {
    res.redirect('/404');
  }
}

exports.composeMessage = function(req, res) {
  var disputeID = sanitize(String(req.params.disputeID)).trim();
  var message = String(req.body.message).trim();

  if(disputeID && message) {
    if(30 >= message.length || message.length <= 10000) {
      var Dispute = mongoose.model('Dispute');
      Dispute.findOne({'_id': disputeID}).exec(function(err, disputeResult) {
        if(err || disputeResult == '' || disputeResult == '[]' || disputeResult == '{}' || disputeResult == null) {
          res.send(404);
        } else {
          if(disputeResult.verdict == 0) {
            if(disputeResult.worker == req.session.user.username || disputeResult.customer == req.session.user.username) {
              var disputeMessage = mongoose.model('disputeMessage');
              var newDisputeMessage = new disputeMessage();
              newDisputeMessage.username = req.session.user.username;
              newDisputeMessage.disputeID = disputeID;
              newDisputeMessage.date = Date.now();
              newDisputeMessage.message = message;
              newDisputeMessage.save();

              res.redirect('/dispute/'+disputeID);
            } else {
              res.redirect('/404');
            }
          } else {
            res.redirect('/dispute/'+disputeID); 
          }
        }
      });
    } else {

    }
  } else {
    res.redirect('/404');
  }
}

exports.apiMessageDispute = function(req, res) {
  var disputeID = sanitize(String(req.params.disputeID)).trim();
  if(disputeID) {
    var Dispute = mongoose.model('Dispute');
    Dispute.findOne({'_id': disputeID}).sort({'date': -1}).limit(50).exec(function(err, disputeResult) {
      if(err || disputeResult == '' || disputeResult == '{}' || disputeResult == '[]' || disputeResult == null) {
        res.redirect('/404');
      } else {
        var disputeMessage = mongoose.model('disputeMessage');
        disputeMessage.find({'disputeID': disputeID}).exec(function(err, result) {
          if(err) {
            res.send(404);
          } else {
            res.send(result);
          }
        });
      }
    });
  } else {
    res.redirect('/404');
  }
}

exports.apiVotes = function(req, res) {
  var disputeID = sanitize(req.params.disputeID);
  if(disputeID) {
    var disputeVote = mongoose.model('disputeVote');
    disputeVote.find({'disputeID': disputeID}).exec(function(err, result) {
      if(err) {
        res.send(404);
      } else {
        res.send(result);
      }
    });
  } else {
    res.redirect('/404');
  }
}

exports.dispute = function(req, res) {
  var disputeID = sanitize(String(req.params.disputeID)).trim();
  if(disputeID) {
    var Dispute = mongoose.model('Dispute');
    Dispute.findOne({'_id': disputeID}).exec(function(err, disputeResult) {
      if(err || disputeResult == '' || disputeResult == '{}' || disputeResult == '[]' || disputeResult == null) {
        res.redirect('/404');
      } else {
        res.render('dispute/dispute', {
          title: 'Dispute | Taskly',
          user: req.session.user,
          worker: disputeResult.worker,
          customer: disputeResult.customer,
          id: disputeID,
          websiteName: 'Taskly'
        });
      }
    });
  } else {
    res.redirect('/404');
  }
};

exports.viewDisputes = function(req, res) {
  res.render('dispute/disputes', {
    title: 'Disputes | ',
    user: req.session.user,
    websiteName: 'Taskly'
  });
}

exports.apiDisputes = function(req, res) {
  var Dispute = mongoose.model('Dispute');
  Dispute.find({'verdict': 0}).sort({'created': -1}).exec(function(err, disputeResult) {
    if(err) {
      res.send(404);
    } else {
      res.send(disputeResult);
    }
  });
}

exports.isVoted = function(req, res) {
  var disputeID = sanitize(req.params.disputeID);
  if(disputeID) {
    var disputeVote = mongoose.model('disputeVote');
    disputeVote.findOne({'disputeID': disputeID, 'username': req.session.user.username}).exec(function(err, result) {
      if(err || result == '' || result == '[]' || result == '{}' || result == null) {
        res.send(String(0));
      } else {
        res.send(String(1));
      }
    });
  } else {
    res.redirect('/404');
  }
}

exports.decide = function(req, res) {
  var disputeID = sanitize(req.params.disputeID);
  if(disputeID) {
    var disputeVote = mongoose.model('disputeVote');
    disputeVote.count({'_id': disputeID}).exec(function(err, disputeCount) {
      if(err) {
        res.send(String('0'));
      } else {
        if(disputeCount >= 17) {
          var Dispute = mongoose.model('Dispute');
          Dispute.findOne({'_id': disputeVote.disputeID}).exec(function(err, disputeResult) {
            if(err || disputeResult == '' || disputeResult == '[]' || disputeResult == '{}' || disputeResult == null) {
              res.send(String('0'));
            } else {
              disputeVote.find({'_id': disputeID}).exec(function(err, disputeVoteResult) {
                if(err || disputeVoteResult == '' || disputeVoteResult == '[]' || disputeVoteResult == '{}' || disputeVoteResult == null) {
                  res.send(String('0'));
                } else { 
                  var total = 0;
                  var worker = 0;
                  var customer = 0;

                  for(var vote in disputeVoteResult) {
                    if(disputeVoteResult[vote]['side'] == worker) {
                      total += disputeVoteResult[vote]['amount'];
                      worker += disputeVoteResult[vote]['amount'];
                    } else {
                      total += disputeVoteResult[vote]['amount'];
                      customer += disputeVoteResult[vote]['amount'];
                    }
                  } 

                  if(worker > customer) {
                    // WORKER WINS

                    // FIND AMOUNT OF SUPPORT FOR WORKER

                    var workers = total / worker;

                    // DISTRIBUTE EARNINGS

                    var customerAmount = customer / workers;

                    // DEDUCT FROM EACH CUSTOMER SIDE

                    for(var vote in disputeVoteResult) {

                      // DEDUCT FROM CUSTOMER SIDE

                      if(disputeVoteResult[vote]['side'] == customer) {
                        var User = mongoose.model('User');
                        User.findOne({'username': disputeVoteResult[vote]['username']}).exec(function(err, userResult) {
                          if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
                            return 0;
                          } else {

                            var newHolding = Number(userResult.holding) - 500;
                            User.update({'username': disputeVoteResult[vote]['username']}, {'holding': newHolding}).exec();

                            // TRANSACTION

                            var Transaction = mongoose.model('Transaction');
                            var newTransaction = new Transaction();
                            newTransaction.from = '';
                            newTransaction.to = disputeVoteResult[vote]['username'];
                            newTransaction.date = Date.now();
                            newTransaction.amount = 500;
                            newTransaction.reason = "Dispute (ID="+disputeID+") lost.";
                            newTransaction.save();

                          }
                        });
                      } else {
                        // REWARD TO SIDE WORKER

                        var User = mongoose.model('User');
                        User.findOne({'username': disputeVoteResult['side']['username']}).exec(function(err, userResult) {
                          if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
                            return 0;
                          } else {

                            var newHolding = Number(userResult.holding) - 500;
                            var newAmount = Number(userResult.taskcoin) + 500 + customerAmount;

                            User.update({'username': disputeVoteResult[vote]['username']}, {'taskcoin': newAmount, 'holding': newHolding}).exec();

                            // TRANSACTION

                            var Transaction = mongoose.model('Transaction');
                            var newTransaction = new Transaction();
                            newTransaction.from = '';
                            newTransaction.to = disputeVoteResult[vote]['username'];
                            newTransaction.date = Date.now();
                            newTransaction.amount = 500 + customerAmount;
                            newTransaction.reason = "Dispute (ID="+disputeID+") won.";
                            newTransaction.save();

                          }
                        });
                      }
                    }

                    // REWARD TO WORKER, DEDUCT FROM CUSTOMER

                    User.findOne({'username': disputeResult.worker}).exec(function(err, userResult) {
                      if(err) {
                        return 0;
                      } else {
                        var newAmount = Number(userResult.taskcoin) + Number(disputeResult.amount);
                        var User = mongoose.model('User');
                        User.update({'username': disputeResult.worker}, {'taskcoin': newAmount}).exec();

                        // TRANSACTION
                        var Transaction = mongoose.model('Transaction');
                        var newTransaction = new Transaction();
                        newTransaction.from = '';
                        newTransaction.to = disputeResult.worker;
                        newTransaction.date = Date.now();
                        newTransaction.amount = disputeResult.amount;
                        newTransaction.reason = "Your side of the dispute (ID="+disputeID+") has won.";
                        newTransaction.save();

                      }
                    });

                    User.findOne({'username': disputeResult.customer}).exec(function(err, userResult) {
                      if(err) {
                        return 0;
                      } else {
                        var newHolding = Number(userResult.holding) - Number(disputeResult.amount);
                        User.update({'username': disputeResult.customer}, {'holding': newHolding}).exec();

                        // TRANSACTION 

                        var Transaction = mongoose.model('Transaction');
                        var newTransaction = new Transaction();
                        newTransaction.from = '';
                        newTransaction.to = disputeResult.customer;
                        newTransaction.date = Date.now();
                        newTransaction.amount = 0 - Number(disputeResult.amount);
                        newTransaction.reason = "Your side of the dispute (ID="+disputeID+") has lost.";
                        newTransaction.save();

                      }
                    });

                    Dispute.update({'_id': disputeID}, {'verdict': 1}).exec();

                    var Deal = mongoose.model('Deal');
                    Deal.update({'_id': disputeResult.dealID}, {'completed': 1, 'feedback': 1}).exec();

                    res.send(String('1'));
                  } else {
                    // CUSTOMER WINS

                    // FIND AMOUNT OF SUPPORT FOR CUSTOMER

                    var customers = total / customer;

                    // DISTRIBUTE EARNINGS

                    var workerAmount = customer / customers;

                    // DEDUCT FROM EACH WORKER SIDE

                    for(var vote in disputeVoteResult) {

                      // DEDUCT FROM WORKER SIDE

                      if(disputeVoteResult[vote]['side'] == worker) {
                        var User = mongoose.model('User');
                        User.findOne({'username': disputeVoteResult[vote]['username']}).exec(function(err, userResult) {
                          if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
                            return 0;
                          } else {

                            var newHolding = Number(userResult.holding) - 500;
                            var User = mongoose.model('User');
                            User.update({'username': disputeVoteResult[vote]['username']}, {'holding': newHolding}).exec();

                            // TRANSACTION

                            var Transaction = mongoose.model('Transaction');
                            var newTransaction = new Transaction();
                            newTransaction.from = '';
                            newTransaction.to = disputeResult.worker;
                            newTransaction.date = Date.now();
                            newTransaction.amount = disputeResult.amount;
                            newTransaction.reason = "Dispute (ID="+disputeID+") lost.";
                            newTransaction.save();

                          }
                        });
                      } else {
                        // REWARD TO SIDE CUSTOMER

                        var User = mongoose.model('User');
                        User.findOne({'username': disputeVoteResult['side']['username']}).exec(function(err, userResult) {
                          if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
                            return 0;
                          } else {

                            var newHolding = Number(userResult.holding) - 500;
                            var newAmount = Number(userResult.taskcoin) + 500 + workerAmount;

                            var User = mongoose.model('User');
                            User.update({'username': disputeVoteResult[vote]['username']}, {'taskcoin': newAmount, 'holding': newHolding}).exec();

                            // TRANSACTION

                            var Transaction = mongoose.model('Transaction');
                            var newTransaction = new Transaction();
                            newTransaction.from = '';
                            newTransaction.to = disputeVoteResult[vote]['username'];
                            newTransaction.date = Date.now();
                            newTransaction.amount = 500 + workerAmount;
                            newTransaction.reason = "Dispute (ID="+disputeID+") won.";
                            newTransaction.save();

                          }
                        });
                      }
                    }

                    // REWARD TO CUSTOMER, DEDUCT FROM WORKER

                    User.findOne({'username': disputeResult.customer}).exec(function(err, userResult) {
                      if(err) {
                        return 0;
                      } else {
                        var newAmount = Number(userResult.taskcoin) + Number(disputeResult.amount);
                        
                        var User = mongoose.model('User');
                        User.update({'username': disputeResult.customer}, {'taskcoin': newAmount}).exec();

                        // TRANSACTION

                        var Transaction = mongoose.model('Transaction');
                        var newTransaction = new Transaction();
                        newTransaction.from = '';
                        newTransaction.to = disputeResult.customer;
                        newTransaction.date = Date.now();
                        newTransaction.amount = disputeResult.amount;
                        newTransaction.reason = "Your side of the dispute (ID="+disputeID+") has won.";
                        newTransaction.save();

                      }
                    });

                    User.findOne({'username': disputeResult.worker}).exec(function(err, userResult) {
                      if(err) {
                        return 0;
                      } else {
                        var newHolding = Number(userResult.holding) - Number(disputeResult.amount);
                        User.update({'username': disputeResult.worker}, {'holding': newHolding}).exec();

                        // TRANSACTION 

                        var Transaction = mongoose.model('Transaction');
                        var newTransaction = new Transaction();
                        newTransaction.from = '';
                        newTransaction.to = disputeResult.worker;
                        newTransaction.date = Date.now();
                        newTransaction.amount = disputeResult.amount;
                        newTransaction.reason = "Your side of the dispute (ID="+disputeID+") has lost.";
                        newTransaction.save();
                      }
                    });

                    var Deal = mongoose.model('Deal');
                    Deal.update({'_id': disputeResult.dealID}, {'completed': 1, 'feedback': 1}).exec();

                    Dispute.update({'_id': disputeID}, {'verdict': 2}).exec();

                    res.send(String('1'));
                  }

                }
              });
            }
          });
        } else {
          res.send(String('0'));
        }
      }
    });
  } else {
    res.redirect('/404');
  }
}

var mongoose = require('mongoose');
var User = require('../db/user');
var Listing = require('../db/listing');
var Watchlist = require('../db/watchlist');
var Deal = require('../db/deal');
var Offer = require('../db/offer');
var Transaction = require('../db/transaction');
var Feedback = require('../db/feedback');
var userActivity = require('../db/useractivity');
var sanitize = require('strip-js');
var passwordHash = require('password-hash');
var twoFactor = require('node-2fa');
var lib = require('../config/lib.js');

exports.signIn = function(req, res) {
	res.render('user/login', {
		title: 'Sign In | Taskly',
		reason: '',
		websiteName: 'Taskly'
	});
}

exports.signUp = function(req, res) {
	res.render('user/register', {
		title: 'Sign Up | Taskly',
		reason: '',
		websiteName: 'Taskly'
	});
}

// POST REGISTER

exports.apiSignIn = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.body.username)).replace(/[^a-z0-9]/gi,'');
	var password = sanitize(lib.removeNullsAndTrim(req.body.password));
	var authentication = sanitize(lib.removeNullsAndTrim(req.body.authentication));

	if(username && password) {
		if(username.length >= 3 || username.length <= 30 && username != 'taskcoin') {
			if(password.length >= 8 || password.length <= 100) {
				var User = mongoose.model('User');
				User.findOne({'username': username}).exec(function(err, userResult) {
					if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
						// NO USER

						res.render('user/login', {
							title: 'Sign In | Taskly',
							reason: 'Username not found',
							websiteName: 'Taskly'
						});
					} else {

						function authenticationError() {
							res.render('user/login', {
								title: 'Sign In | Taskly',
								reason: 'Two factor authentication error',
								websiteName: 'Taskly'
							});
						}

						if(userResult.authenticationEnabled) {
							if(userResult.authenticationEnabled == 1) {
								if(authentication) {
									if(authentication.length == 6) {
										var clientToken = Number(authentication);
										var serverToken = twoFactor.generateToken(userResult.authenticationSecret)['token'];

										if(clientToken == serverToken) {
											if(userResult.role == 0) {
												// BANNED
												res.render('user/login', {
													title: 'Sign In | Taskly',
													reason: 'User has been banned',
													websiteName: 'Taskly'
												});
											} else {
												if(passwordHash.verify(password, userResult.password) == true) {
													req.session.user = userResult;

													// NEW USER ACTIVITY (IP & USERAGENT)

													var userActivity = mongoose.model('userActivity');

													userActivity.findOne({'type': 'user agent', 'value': req.get('user-agent'), 'username': username}).exec(function(err, userActivityResult) {
														if(err || userActivityResult == '' || userActivityResult == '[]' || userActivityResult == '{}' || userActivityResult == null) {
															var newUserActivity = new userActivity();
															newUserActivity.username = username;
															newUserActivity.date = Date.now();
															newUserActivity.type = 'user agent';
															newUserActivity.value = req.get('user-agent');
															newUserActivity.save();
														} else {
															return 1;
														}
													});

													userActivity.findOne({'type': 'IP', 'value': req.ip, 'username': username}).exec(function(err, userActivityResult) {
														if(err || userActivityResult == '' || userActivityResult == '[]' || userActivityResult == '{}' || userActivityResult == null) {
															var newUserActivity = new userActivity();
															newUserActivity.username = username;
															newUserActivity.date = Date.now();
															newUserActivity.type = 'IP';
															newUserActivity.value = req.ip;
															newUserActivity.save();
														} else {
															return 1;
														}
													});

													res.redirect('/');
												} else {
													// WRONG PASSWORD

													res.render('user/login', {
														title: 'Sign In | Taskly',
														reason: 'Incorrect password',
														websiteName: 'Taskly'
													});

												}
											}
										} else {
											authenticationError();
										}
									} else {
										authenticationError();
									}
								} else {
									authenticationError();
								}
							} else {
								authenticationError();
							}
						} else {
							if(userResult.role == 0) {
								// BANNED
								res.render('user/login', {
									title: 'Sign In | Taskly',
									reason: 'User has been banned',
									websiteName: 'Taskly'
								});
							} else {
								if(passwordHash.verify(password, userResult.password) == true) {
									req.session.user = userResult;

									// NEW USER ACTIVITY (IP & USERAGENT)

									var userActivity = mongoose.model('userActivity');

									userActivity.findOne({'type': 'user agent', 'value': req.get('user-agent'), 'username': username}).exec(function(err, userActivityResult) {
										if(err || userActivityResult == '' || userActivityResult == '[]' || userActivityResult == '{}' || userActivityResult == null) {
											var newUserActivity = new userActivity();
											newUserActivity.username = username;
											newUserActivity.date = Date.now();
											newUserActivity.type = 'user agent';
											newUserActivity.value = 'New user agent... ' + req.get('user-agent');
											newUserActivity.save();
										} else {
											return 1;
										}
									});

									userActivity.findOne({'type': 'IP', 'value': req.ip, 'username': username}).exec(function(err, userActivityResult) {
										if(err || userActivityResult == '' || userActivityResult == '[]' || userActivityResult == '{}' || userActivityResult == null) {
											var newUserActivity = new userActivity();
											newUserActivity.username = username;
											newUserActivity.date = Date.now();
											newUserActivity.type = 'IP';
											newUserActivity.value = 'New IP... ' + req.ip;
											newUserActivity.save();
										} else {
											return 1;
										}
									});

									res.redirect('/');
								} else {
									// WRONG PASSWORD

									res.render('user/login', {
										title: 'Sign In | Taskly',
										reason: 'Incorrect password',
										websiteName: 'Taskly'
									});

								}
							}
						}
					}
				});
			} else {
				// PASSWORD TOO SHORT OR LONG
				res.render('user/login', {
					title: 'Sign In | Taskly',
					reason: 'Ensure that the password is between 8 and 100 characters',
					websiteName: 'Taskly'
				});
			}
		} else {

			// USERNAME TOO SHORT OR LONG
			res.render('user/login', {
				title: 'Sign In | Taskly',
				reason: 'Username must be between 3 and 30 characters',
				websiteName: 'Taskly'
			});
		}
	} else {
		res.redirect('/sign-up');
	}
}

exports.apiSignUp = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.body.username)).replace(/[^a-z0-9]/gi,'');;
	var password = sanitize(lib.removeNullsAndTrim(req.body.password));
	var confirm = sanitize(lib.removeNullsAndTrim(req.body.confirm));
	var email = req.body.email;
	var referral = sanitize(lib.removeNullsAndTrim(req.body.referral));
	var ip = req.ip;
	var userAgent = req.get('user-agent');
	var date = Date.now();

	var User = mongoose.model('User');

	if(username && password && confirm) {
		if(username.length >= 3 && username.length <= 30 && username != 'taskcoin') {
			if(password.length >= 8 && password.length <= 100) {
				if(password == confirm) {
					var User = mongoose.model('User');
					User.findOne({'username': username}).select('_id username').exec(function(err, usernameResult) {
						if(err || usernameResult) {
							// USERNAME ALREADY EXISTS

							res.render('user/register', {
								title: 'Sign Up | Taskly',
								reason: 'Username already exists',
								websiteName: 'Taskly'
							});

						} else {

							var newUser = new User();
							newUser.username = username;
							newUser.password = passwordHash.generate(password);
							newUser.created = date;
							newUser.biography = 'New user';
							newUser.userAgent = userAgent;
							newUser.ip = ip;
							newUser.role = 1;
							newUser.taskcoin = 2500;
							newUser.holding = 0;
							newUser.referral = '';

							User.findOne({'username': 'taskcoin'}).exec(function(err, result) {
								if(err || result == '' || result == '[]' || result == '{}' || result == null) {
									var newUser = new User();
									newUser.username = 'taskcoin';
									newUser.role = 0;
									newUser.taskcoin = 25000000000 - 2500;
									newUser.save();
								} else {	
									// DEDUCT FROM TAX ACCOUNT

									User.findOne({'username': 'taskcoin'}).select('_id username taskcoin').exec(function(err, result) {
										if(err) {
											return false;
										} else {
											var newAmount = Number(result.taskcoin) - 2500;
											User.update({'username': 'taskcoin'}, {'taskcoin': newAmount}).exec();
										}
									});

									// CREATE TRANSACTION

									lib.newTransactionNoHolding(username, 'taskcoin', 2500, 'New account created ('+username+'). Welcome grant.');
								}
							});		

							newUser.save(function(err, save) {
								if(err) {
									res.render('user/register', {
										title: 'Sign Up | Taskly',
										reason: 'Problem registering. Try again',
										websiteName: 'Taskly'
									});
								} else {
									if(save) {
										User.findOne({'username': username}).exec(function(err, result) {
											req.session.user = result;
											res.redirect('/');
										});
									} else {
										res.render('user/register', {
											title: 'Sign Up | Taskly',
											reason: 'Problem registering. Try again',
											websiteName: 'Taskly'
										});
									}
								}
							});
						}
					});
				} else {
					res.render('user/register', {
						title: 'Sign Up | Taskly',
						reason: 'Password and confirmation password do not match',
						websiteName: 'Taskly'
					});
				}
			} else {
				// PASSWORD TOO SHORT
				res.render('user/register', {
					title: 'Sign Up | Taskly',
					reason: 'Password must be between 8 and 100 characters',
					websiteName: 'Taskly'
				});

			}
		} else {
			// USERNAME TOO SHORT

			res.render('user/register', {
				title: 'Sign Up | Taskly',
				reason: 'Username must be between 3 and 30 characters',
				websiteName: 'Taskly'
			});
		}
	} else {
		res.redirect('/sign-up');
	}
}

exports.signOut = function(req, res) {
	if(req.session.user && req.cookies.user_sid) {
		res.clearCookie('user_sid');
		res.redirect('/');
	} else {
		res.redirect('/sign-in');
	}
}

exports.redirectToProfile = function(req, res) {
	res.redirect('/u/'+req.session.user.username);
}

exports.u = function(req, res) {
	var username = lib.removeNullsAndTrim(req.params.username);
	if(/^[a-zA-Z0-9]+$/.test(username) == true || username) {
		var User = mongoose.model('User');
		User.findOne({'username': username}, function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.redirect('/404');
			} else if (result.role == 0) {
				res.redirect('/404');
			} else {
				res.render('u', {
					title: result.username + ' | Taskly',
					user: req.session.user,
					profileUsername: username,
					profileImage: result.photo,
					profileBio: result.biography,
					profileJoined: String(result.created).slice(4,15),
					websiteName: 'Taskly'
				});
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.serviceCount = function(req, res) {
	var username = lib.removeNullsAndTrim(req.params.username);
	if(/^[a-zA-Z0-9]+$/.test(username) == true || username) {
		var Listing = mongoose.model('Listing');
		Listing.count({'username': username, 'public': 1, 'type': 1}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.send(String('0'));
			} else {
				res.send(String(listingResult));
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.requestCount = function(req, res) {
	var username = lib.removeNullsAndTrim(req.params.username);
	if(/^[a-zA-Z0-9]+$/.test(username) == true || username) {
		var Listing = mongoose.model('Listing');
		Listing.count({'username': username, 'public': 1, 'type': 0}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.send(String('0'));
			} else {
				res.send(String(listingResult));
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.activeJobsCount = function(req, res) {
	var Deal = mongoose.model('Deal');
	Deal.count({'worker': req.session.user.username, 'completed': 0}).exec(function(err, dealResult) {
		if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
			res.send(String('0'));
		} else {
			res.send(String(dealResult));
		}
	});
}

exports.profileRequests = function(req, res) {
	var username = lib.removeNullsAndTrim(req.params.username);
	if(/^[a-zA-Z0-9]+$/.test(username) == true || username) {
		var Listing = mongoose.model('Listing');
		Listing.find({'username': username, 'public': 1, 'type': 0}).limit(3).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.send(String('0'));
			} else {
				res.send(listingResult);
			}
		});
	} else {
		res.redirect('/404');
	}
}	

exports.profileServices = function(req, res) {
	var username = lib.removeNullsAndTrim(req.params.username);
	if(/^[a-zA-Z0-9]+$/.test(username) == true || username) {
		var Listing = mongoose.model('Listing');
		Listing.find({'username': username, 'public': 1, 'type': 1}).limit(3).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.send(String('0'));
			} else {
				res.send(listingResult);
			}
		});
	} else {
		res.redirect('/404');
	}
}

// USER 

exports.dashboard = function(req, res) {
	res.render('user/dashboard', {
		title: 'Dashboard | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.message = function(req, res) {
	var pageNumber = sanitize(Number(req.query.p));
	if(pageNumber) {
		if(pageNumber >= 1 && pageNumber <= 20) {
			res.render('user/messages', {
				title: 'Messages | Taskly',
				user: req.session.user,
				websiteName: 'Taskly',
				pageBack: pageNumber - 1,
				pageOn: pageNumber,
				pageNext: pageNumber + 1
			});
		} else {
			res.render('user/messages', {
				title: 'Messages | Taskly',
				user: req.session.user,
				websiteName: 'Taskly',
				pageBack: 1,
				pageOn: 1,
				pageNext: 2
			});
		}
	} else {
		res.render('user/messages', {
			title: 'Messages | Taskly',
			user: req.session.user,
			websiteName: 'Taskly',
			pageBack: 1,
			pageOn: 1,
			pageNext: 2
		});
	}
}

exports.apiMessages = function(req, res) {
	var pageNumber = sanitize(Number(req.query.p));

	if(pageNumber) {
		if(pageNumber >= 1 && pageNumber <= 20) {
			var Message = mongoose.model('Message');
			Message.paginate({'to': req.session.user.username}, {page: pageNumber, limit: 25}).sort({'date': -1}).exec(function(err, result) {
				if(err || result == '' || result == '[]' || result == '{}' || result == null) {
					res.send(String('0'));
				} else {
					res.send(result);
				}
			});
		} else {
			var Message = mongoose.model('Message');
			Message.find({'to': req.session.user.username}).sort({'date': -1}).limit(25).exec(function(err, result) {
				if(err || result == '' || result == '[]' || result == '{}' || result == null) {
					res.send(String('0'));
				} else {
					res.send(result);
				}
			});
		}
	} else {
		var Message = mongoose.model('Message');
		Message.find({'to': req.session.user.username}).sort({'date': -1}).limit(25).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {
				res.send(result);
			}
		});
	}	
}

exports.composeMessage = function(req, res) {
	var username = lib.removeNullsAndTrim(req.query.username);
	var title = String(req.query.title).trim();

	if(username && title) {
		res.render('user/compose', {
			title: 'Compose | Taskly',
			user: req.session.user,
			websiteName: 'Taskly',
			reason: '',
			messageTitle: title,
			messageUsername: username
		});
	} else {	
		res.render('user/compose', {
			title: 'Compose | Taskly',
			user: req.session.user,
			websiteName: 'Taskly',
			reason: '',
			messageTitle: '',
			messageUsername: ''
		});
	}
}

exports.sendMessage = function(req, res) {
	var username = lib.removeNullsAndTrim(req.body.username);
	var title = String(req.body.title).trim();
	var message = String(req.body.message).trim();	

	function error(reason) {
		res.render('user/compose', {
			title: 'Compose | Taskly',
			user: req.session.user,
			websiteName: 'Taskly',
			reason: reason,
			messageTitle: '',
			messageUsername: ''
		});
	}

	if(username && title && message) {
		if(username.length >= 5 && username.length <= 30) {
			if(title.length >= 10 && title.length <= 100) {
				if(message.length >= 30 && title.length <= 2000) {
					if(username == req.session.user.username) {
						error('Can\'t send a message to yourself');
					} else {
						var User = mongoose.model('User');
						User.findOne({'username': username}).select('_id username').exec(function(err, userResult) {
							if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
								error('Username doesn\'t exist.');
							} else {
								var Message = mongoose.model('Message');
								var newMessage = new Message();
								newMessage.from = req.session.user.username;
								newMessage.to = username;
								newMessage.title = title;
								newMessage.message = message;
								newMessage.read = 0;
								newMessage.date = Date.now();
								newMessage.save();

								res.redirect('/user/messages/sent');
							}
						});
					}
				} else {	
					error('Message has to be between 30 and 2,000 characters');
				}
			} else {
				error('Title has to be between 10 and 100 characters');
			}
		} else {
			error('Username has to be between 5 and 30 characters');
		}
	} else {
		res.redirect('/user/message/compose');
	}
}

exports.markAsRead = function(req, res) {
	var messageID = sanitize(lib.removeNullsAndTrim(req.params.messageID));
	if(messageID) {
		var Message = mongoose.model('Message');
		Message.findOne({'_id': messageID, 'to': req.session.user.username}).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.redirect('/404');
			} else {
				Message.update({'_id': messageID, 'to': req.session.user.username}, {'read': 1}).exec();

				res.redirect('/user/messages');
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.sentMessages = function(req, res) {
	var pageNumber = sanitize(Number(req.query.p));

	if(pageNumber) {
		if(pageNumber >= 1 && pageNumber <= 20) {	
			res.render('user/messagessent', {
				title: 'Messages Sent | Taskly',
				user: req.session.user,
				websiteName: 'Taskly',
				pageBack: pageNumber - 1,
				pageOn:  pageNumber,
				pageNext:  pageNumber + 1
			});
		} else {
			res.render('user/messagessent', {
				title: 'Messages Sent | Taskly',
				user: req.session.user,
				websiteName: 'Taskly',
				pageBack: 1,
				pageOn: 1,
				pageNext: 2
			});
		}
	} else {
		res.render('user/messagessent', {
			title: 'Messages Sent | Taskly',
			user: req.session.user,
			websiteName: 'Taskly',
			pageBack: 1,
			pageOn: 1,
			pageNext: 2
		});
	}
}

exports.apiSentMessages = function(req, res) {
	var pageNumber = sanitize(Number(req.query.p));

	if(pageNumber) {
		if(pageNumber >= 1 && pageNumber <= 20) {	
			var Message = mongoose.model('Message');
			Message.paginate({'from': req.session.user.username}, {page: pageNumber, limit: 25}).sort({'date': -1}).exec(function(err, result) {
				if(err || result == '' || result == '[]' || result == '{}' || result == null) {
					res.send(String('0'));
				} else {
					res.send(result);
				}	
			});
		} else {
			var Message = mongoose.model('Message');
			Message.find({'from': req.session.user.username}).limit(25).sort({'date': -1}).exec(function(err, result) {
				if(err || result == '' || result == '[]' || result == '{}' || result == null) {
					res.send(String('0'));
				} else {
					res.send(result);
				}
			});
		}
	} else {
		var Message = mongoose.model('Message');	
		Message.find({'from': req.session.user.username}).limit(25).sort({'date': -1}).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {
				res.send(result);	
			}
		});
	}
}

exports.wallet = function(req, res) {
	res.render('user/wallet', {
		title: 'Wallets | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.transactionsAPI = function(req, res) {
	var Transaction = mongoose.model('Transaction');
	Transaction.find({'to': req.session.user.username}).sort({'date': -1}).exec(function(err, transactionResult) {
		if(err || transactionResult == '' || transactionResult == '[]' || transactionResult == '{}' || transactionResult == null) {
			res.send(404);
		} else {
			res.send(transactionResult);
		}
	});
}

exports.settings = function(req, res) {
	if(req.session.user) {
		var User = mongoose.model('User');
		User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
			if(!userResult.authenticationEnabled) {
				res.render('user/settings', {
					title: 'Settings | Taskly',
					user: req.session.user,
					websiteName: 'Taskly',
					twoFactorStatus: false,
					twoFactorSecret: '',
					bio: userResult.biography	
				});
			} else {
				if(userResult.authenticationEnabled == 0) {
					res.render('user/settings', {
						title: 'Settings | Taskly',
						user: req.session.user,
						websiteName: 'Taskly',
						twoFactorStatus: false,
						twoFactorSecret: newSecret.qr,
						bio: userResult.biography
					});
				} else {
					res.render('user/settings', {
						title: 'Settings | Taskly',
						user: req.session.user,
						websiteName: 'Taskly',
						twoFactorStatus: true,
						twoFactorSecret: userResult.authenticationSecret,
						bio: userResult.biography
					});
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.recentOffers = function(req, res) {
	var Offers = mongoose.model('Offer');
	Offers.find({'worker': req.session.user.username}).limit(5).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(String(0));
		} else {
			res.send(result);
		}
	});
}

exports.apiActivity = function(req, res) {
	var userActivity = mongoose.model('userActivity');
	userActivity.find({'username': req.session.user.username}).limit(10).sort({'date': -1}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.changePassword = function(req, res) {
	var oldPassword = lib.removeNullsAndTrim(req.body.oldPassword);
	var newPassword = lib.removeNullsAndTrim(req.body.newPassword);

	var User = mongoose.model('User');
	if(oldPassword && newPassword) {
		if(oldPassword.length >= 8 && oldPassword.length <= 100) {
			User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
				if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
					res.redirect('/404');
				} else {	
					if(passwordHash.verify(oldPassword, userResult.password) == true) {
						if(newPassword.length >= 8 && newPassword.length <= 100) {
							User.update({'username': req.session.user.username}, {'password': passwordHash.generate(newPassword)}).exec();

							res.redirect('/sign-out');
						} else {
							res.redirect('/user/settings');
						}
					} else {
						res.redirect('/user/settings');
					}
				}
			});
		} else {
			res.redirect('/user/settings');
		}
	} else {
		res.redirect('/404');
	}
}

exports.changeBio = function(req, res) {
	var bio = String(req.body.bio).trim();
	if(bio) {
		if(bio.length >= 30 && bio.length <= 10000) {
			var User = mongoose.model('User');
			User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
				if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
					res.redirect('/404');
				} else {
					User.update({'username': req.session.user.username}, {'biography': bio}).exec();

					res.redirect('/u/'+req.session.user.username);
				}
			});
		} else {
			res.redirect('/user/settings');
		}
	} else {
		res.redirect('/404');
	}
}

exports.generateTwoFactorSecret = function(req, res) {
	var User = mongoose.model('User');
	User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
		if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
			res.redirect('/404');
		} else {
			if(!userResult.authenticationSecret) {
				var newSecret = twoFactor.generateSecret({name: 'Taskly', account: req.session.user.username});

				User.update({'username': req.session.user.username}, {'authenticationSecret': newSecret.secret}).exec();

				res.send(newSecret.qr);
			} else {
				if(userResult.authenticationEnabled == 1) {
					res.redirect('/user/settings');
				} else {
					var newSecret = twoFactor.generateSecret({name: 'Taskly', account: req.session.user.username});

					User.update({'username': req.session.user.username}, {'authenticationSecret': newSecret.secret}).exec();

					res.send(newSecret.qr);
				}
			}
		}
	});
}

exports.twoFactorEnable = function(req, res) {
	var token = lib.removeNullsAndTrim(req.body.token);
	if(token) {
		if(token.length == 6) {
			var User = mongoose.model('User');
			User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
				if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
					res.redirect('/sign-in');
				} else {
					if(userResult.authenticationSecret) {
						var serverToken = Number(twoFactor.generateToken(userResult.authenticationSecret)['token']);
						var clientToken = Number(token);

						if(serverToken == clientToken) {
							User.update({'username': req.session.user.username}, {'authenticationEnabled': true}).exec();
							res.redirect('/user/settings');
						} else {
							res.redirect('/user/settings');
						}

					} else {
						res.redirect('/user/settings');
					}
				}
			});
		} else {
			res.redirect('/user/settings');
		}
	} else {
		res.redirect('/404');
	}
}

exports.twoFactorDisable = function(req, res) {
	var token = lib.removeNullsAndTrim(req.body.token);
	if(token) {
		if(token.length == 6) {
			var User = mongoose.model('User');
			User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
				if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
					res.redirect('/sign-in');
				} else {
					if(userResult.authenticationSecret) {
						var serverToken = Number(twoFactor.generateToken(userResult.authenticationSecret)['token']);
						var clientToken = Number(token);

						if(serverToken == clientToken) {
							User.update({'username': req.session.user.username}, {'authenticationEnabled': false, 'authenticationSecret': ''}).exec();

							res.redirect('/user/settings');
						} else {
							res.redirect('/user/settings');
						}
					} else {
						res.redirect('/user/settings');
					}
				}
			});
		} else {
			res.redirect('/user/settings');
		}
	} else {
		res.redirect('/404');
	}
}

// CUSTOMER

exports.requests = function(req, res) {
	res.render('user/requests/requests', {
		title: 'Requests | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.apiRequests = function(req, res) {
	var Listing = mongoose.model('Listing');
	Listing.find({'username': req.session.user.username, 'type': 0, 'public': 1}).sort({'created': -1}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.requestWatchlist = function(req, res) {
	res.render('user/requests/watchlist', {
		title: 'Watchlist | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.apiRequestWatchlist = function(req, res) {
	var Watchlist = mongoose.model('Watchlist');
	Watchlist.find({'username': req.session.user.username}).limit(12).sort({'date': -1}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(String('0'));
		} else {
			res.send(result);
		}
	});
}

exports.requestWatchlistRemove = function(req, res) {
	var id = sanitize(lib.removeNullsAndTrim(req.params.id));
	if(id) {
		var Watchlist = mongoose.model('Watchlist');
		Watchlist.remove({'_id': id, 'username': req.session.user.username}).exec();

		res.redirect('/user/requests/watchlist');
	} else {	
		res.redirect('/404');
	}
}

exports.requestDeals = function(req, res) {
	res.render('user/requests/deals', {
		title: 'Active Deals | ',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.apiRequestDeals = function(req, res) {
	var Deal = mongoose.model('Deal');
	Deal.find({'customer': req.session.user.username, 'completed': false}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});	
}	

exports.requestCompleted = function(req, res) {
	res.render('user/requests/completeddeals', {
		title: 'Completed Deals | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.requestCompletedAPI = function(req, res) {
	var Deal = mongoose.model('Deal');
	Deal.find({'customer': req.session.user.username, 'completed': true}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});	
}

exports.requestOffers = function(req, res) {
	res.render('user/requests/offers', {
		title: 'Offers Made | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.apiRequestOffers = function(req, res) {
	var Offer = mongoose.model('Offer');
	Offer.find({'customer': req.session.user.username}).sort({'created': -1}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(String('0'));
		} else {
			res.send(result);
		}
	});
}

exports.requestDisputes = function(req, res) {
	res.render('user/requests/disputes', {
		title: 'Requests Disputes | ',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.apiRequestDisputes = function(req, res) {
	var Disputes = mongoose.model('Dispute');
	Disputes.find({'customer': req.session.user.username}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(String('0'));
		} else {
			res.send(result);
		}
	});
}

// WORKER

exports.services = function(req, res) {
	res.render('user/services/services', {
		title: 'Services | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});	
}

exports.apiServices = function(req, res) {
	var Listing = mongoose.model('Listing');
	Listing.find({'username': req.session.user.username, 'type': 1, 'public': true}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.serviceJobs = function(req, res) {
	res.render('user/services/jobs', {
		title: 'Service Jobs | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.apiServiceJobs = function(req, res) {
	var Deal = mongoose.model('Deal');
	Deal.find({'worker': req.session.user.username, 'completed': false}).sort({'date': -1}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.serviceComplete = function(req, res) {
	res.render('user/services/complete', {
		title: 'Service Completed | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.apiServiceComplete = function(req, res) {
	var Listing = mongoose.model('Listing');
	Listing.find({'username': req.session.user.username, 'public': false}).sort({'created': -1}).exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.completedJobs = function(req, res) {
	res.render('user/services/completejob', {
		title: 'Jobs Completed | ',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.apiCompletedJobs = function(req, res) {
	var Deal = mongoose.model('Deal');
	Deal.find({'worker': req.session.user.username, 'completed': true}).exec(function(err, result) {
		if(err || result == '' || result == '{}' || result == '[]' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.serviceDispute = function(req, res) {
	res.render('user/services/dispute', {
		title: 'Service Dispute | Taskly',
		user: req.session.user,
		websiteName: 'Taskly'
	});
}

exports.apiServiceDispute = function(req, res) {
	var Dispute = mongoose.model('Dispute');
	Dispute.find({'worker': req.session.user.username}).exec(function(err, result){
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.send(404);
		} else {
			res.send(result);
		}
	});
}

exports.feedbackCount = function(req, res) {
	var username = sanitize(req.params.username);
	if(username) {
		var Feedback = mongoose.model('Feedback');
		Feedback.count({'worker': username}).exec(function(err, result) {
			res.send(String(result));
		});
	} else {
		res.send('/404');
	}
}

exports.threebackAPI = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.params.username));
	var listing = sanitize(lib.removeNullsAndTrim(req.query.listingID));

	if(username && listing) {
		var Feedback = mongoose.model('Feedback');
		Feedback.find({'worker': username, 'listingID': new RegExp(listing)}).limit(12).exec(function(err, result) {
			if(err || result == '' || result == '[]' || result == '{}' || result == null) {
				res.send(String('0'));
			} else {	
				res.send(result);
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.servicesCount = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.params.username));
	if(username) {
		var Listing = mongoose.model('Listing');
		Listing.count({'username': username, 'type': 1}).exec(function(err, result) {
			res.send(String(result));
		});
	} else {
		res.send('/404');
	}
}

exports.requestsCount = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.params.username));
	if(username) {
		var Listing = mongoose.model('Listing');
		Listing.count({'username': username, 'type': 0}).exec(function(err, result) {
			res.send(String(result));
		});
	} else {
		res.send('/404');
	}
}

exports.userFeedback = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.params.username));
	var pageNumber = sanitize(Number(req.query.p));

	if(username) {
		var User = mongoose.model('User');
		User.findOne({'username': username}).select('_id username created biography photo').exec(function(err, userResult) {
			if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
				res.redirect('/404');
			} else {
				if(pageNumber) {
					if(pageNumber >= 1 && pageNumber <= 20) {
						res.render('user/feedback', {
							title: 'Feedback | Taskly',
							user: req.session.user,
							profileUsername: userResult.username,
							profileImage: userResult.photo,
							profileBio: userResult.biography,
							profileJoined: String(userResult.created).slice(4,15),
							pageBack: pageNumber - 1,
							pageOn: pageNumber,
							pageNext: pageNumber +1,
							websiteName: 'Taskly'
						});
					} else {
						res.render('user/feedback', {
							title: 'Feedback | Taskly',
							user: req.session.user,
							profileUsername: userResult.username,
							profileImage: userResult.photo,
							profileBio: userResult.biography,
							profileJoined: String(userResult.created).slice(4,15),
							pageBack: 1,
							pageOn: 1,
							pageNext: 2,
							websiteName: 'Taskly'
						});
					}
				} else {	
					res.render('user/feedback', {
						title: 'Feedback | Taskly',
						user: req.session.user,
						profileUsername: userResult.username,
						profileImage: userResult.photo,
						profileBio: userResult.biography,
						profileJoined: String(userResult.created).slice(4,15),
						pageBack: 1,
						pageOn: 1,
						pageNext: 2,
						websiteName: 'Taskly'
					});
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.userFeedbackAPI = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.params.username));
	var pageNumber = sanitize(Number(req.query.p));

	if(username) {
		var User = mongoose.model('User');
		User.findOne({'username': username}).select('_id username').exec(function(err, userResult) {
			if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
				res.redirect('/404');
			} else {
				if(pageNumber) {
					var Feedback = mongoose.model('Feedback');
					Feedback.paginate({'worker': username}, {page: pageNumber, limit: 12}, function(err, result) {
						if(err || result == '' || result == '[]' || result == '{}' || result == null) {
							res.send(String('0'));
						} else {	
							res.send(result);
						}
					});
				} else {
					var Feedback = mongoose.model('Feedback');
					Feedback.find({'worker': username}).limit(12).exec(function(err, result) {
						if(err || result == '' || result == '[]' || result == '{}' || result == null) {
							res.send(String('0'));
						} else {	
							res.send(result);
						}
					});
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.trust = function(req, res) {
	var User = mongoose.model('User');
	User.findOne({'username': req.session.user.username}).select('_id username taskcoin').exec(function(err, result) {
		if(err || result == '' || result == '[]' || result == '{}' || result == null) {
			res.redirect('/404');
		} else {
			if(Number(result.taskcoin) >= 3000) {
				var username = sanitize(req.params.username);
				if(username) {
					var User = mongoose.model('User');
					User.findOne({'username': username}).select('_id username').exec(function(err, result) {
						if(err || result == '' || result == '[]' || result == '{}' || result == null) {
							res.redirect('/404');
						} else {
							res.render('trust', {
								title: username + ' Trust | Taskly',
								user: req.session.user,
								websiteName: 'Taskly'
							});
						}
					});
				} else {
					res.redirect('/404');
				}
			} else {
				res.send(String('You need at least T3,000 to use this functionality.'));
			}
		}
	});
}

exports.trustAPI = function(req, res) {
	var username = sanitize(lib.removeNullsAndTrim(req.params.username));
	if(username) {
		var User = mongoose.model('User');
		User.findOne({'username': username}).select('_id username created ip userAgent').exec(function(err, userResult) {
			if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
				res.redirect('/404');
			} else {	
				res.send(userResult);
			}
		});
	} else {
		res.redirect('/404');
	}
}
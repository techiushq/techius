var mongoose = require('mongoose');
var User = require('../db/user');

module.exports = function(app) {
	function isLoggedIn(req, res, next) {
		if (req.session.user && req.cookies.user_sid) {
			var User = mongoose.model('User');
	    	User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
	    		if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
	    			res.redirect('/sign-in');
	    		} else {
	    			req.session.user = userResult;
	    			next();
	    		}
	    	});
		} else {
			res.redirect('/sign-in');
		}
	}

	function ifLoggedIn(req, res, next) {
	    if (req.session.user && req.cookies.user_sid) {
	        res.redirect('/');
	    } else {
	    	next();
	    }
	};

	// GENERAL

	var general = require('../config/general.js');

	app.get('/', general.index);
	app.get('/api', function(req, res) {
		res.send('coming soon');
	});
	app.get('/about', general.about);
	app.get('/requests', general.requests);
	app.get('/services', general.services);
	app.get('/newlistings/:type', general.newListings);
	app.get('/economy', general.economy);
	app.get('/sitemap', general.sitemap);

	// ACCOUNT

	var user = require('../config/user.js');

	app.get('/sign-in', ifLoggedIn, user.signIn);
	app.get('/sign-up', ifLoggedIn, user.signUp);

	app.get('/sign-out', user.signOut);

	app.post('/sign-in', ifLoggedIn, user.apiSignIn);
	app.post('/sign-up', ifLoggedIn, user.apiSignUp);

	app.get('/u/:username/feedback', user.userFeedback);
	app.get('/api/u/:username/feedback', user.userFeedbackAPI);

	app.get('/api/u/:username/threeback', user.threebackAPI);

	app.get('/api/u/:username/feedbackcount', user.feedbackCount);
	app.get('/u/:username/servicescount', user.serviceCount);
	app.get('/u/:username/requestscount', user.requestCount);
	app.get('/api/activejobs/count', isLoggedIn, user.activeJobsCount);

	app.get('/u/:username/requests', user.profileRequests);
	app.get('/u/:username/services', user.profileServices);

	// USER

	app.get('/user/messages', isLoggedIn, user.message);
	app.get('/api/user/messages', isLoggedIn, user.apiMessages);

	app.get('/user/messages/compose', isLoggedIn, user.composeMessage);
	app.post('/user/messages/compose', isLoggedIn, user.sendMessage);

	app.get('/user/messages/sent', isLoggedIn, user.sentMessages);
	app.get('/api/user/messages/sent', isLoggedIn, user.apiSentMessages);

	app.get('/user/messages/mark/:messageID', isLoggedIn, user.markAsRead);

	app.get('/user', isLoggedIn, user.redirectToProfile);

	app.get('/user/dashboard', isLoggedIn, user.dashboard);

	app.get('/user/wallet', isLoggedIn, user.wallet);
	app.get('/api/wallet', isLoggedIn, user.transactionsAPI);

	app.get('/user/settings', isLoggedIn, user.settings);

	app.post('/user/settings/password', isLoggedIn, user.changePassword);
	app.post('/user/settings/bio', isLoggedIn, user.changeBio);

	app.get('/user/settings/2fa/secret', isLoggedIn, user.generateTwoFactorSecret);
	app.post('/user/settings/2fa/enable', isLoggedIn, user.twoFactorEnable);
	app.post('/user/settings/2fa/disable', isLoggedIn, user.twoFactorDisable);

	app.get('/api/offers', isLoggedIn, user.recentOffers);

	app.get('/api/activity', isLoggedIn, user.apiActivity);

	app.get('/u/:username/trust', isLoggedIn, user.trust);
	app.get('/api/u/:username/trust', isLoggedIn, user.trustAPI);

	// CUSTOMER

	app.get('/user/requests', isLoggedIn, user.requests);
	app.get('/api/user/requests', isLoggedIn, user.apiRequests);
	
	app.get('/user/requests/watchlist', isLoggedIn, user.requestWatchlist);	
	app.get('/api/user/requests/watchlist', isLoggedIn, user.apiRequestWatchlist);

	app.get('/user/watchlist/:id/remove', isLoggedIn, user.requestWatchlistRemove);

	app.get('/user/requests/jobs', isLoggedIn, user.requestDeals);
	app.get('/api/user/requests/jobs', isLoggedIn, user.apiRequestDeals);

	app.get('/user/requests/jobs/completed', isLoggedIn, user.requestCompleted);
	app.get('/api/user/requests/jobs/completed', isLoggedIn, user.requestCompletedAPI);

	app.get('/user/requests/offers', isLoggedIn, user.requestOffers);
	app.get('/api/user/requests/offers', isLoggedIn, user.apiRequestOffers);

	app.get('/user/requests/disputes', isLoggedIn, user.requestDisputes);
	app.get('/api/user/requests/disputes', isLoggedIn, user.apiRequestDisputes);

	// WORKER 

	app.get('/user/services', isLoggedIn, user.services);
	app.get('/api/user/services', isLoggedIn, user.apiServices);

	app.get('/user/services/completed', isLoggedIn, user.serviceComplete);
	app.get('/api/user/services/completed', isLoggedIn, user.apiServiceComplete);

	app.get('/user/services/jobs', isLoggedIn, user.serviceJobs);
	app.get('/api/user/services/jobs', isLoggedIn, user.apiServiceJobs);

	app.get('/user/services/jobs/completed', isLoggedIn, user.completedJobs);
	app.get('/api/user/services/jobs/completed', isLoggedIn, user.apiCompletedJobs);

	app.get('/user/services/disputes', isLoggedIn, user.serviceDispute);
	app.get('/api/user/services/disputes', isLoggedIn, user.apiServiceDispute);

	// Moderation

	var moderation = require('../config/moderation.js');

	app.get('/admin', isLoggedIn, moderation.dashboard);
	app.get('/admin/finances', isLoggedIn, moderation.feeChange);
	app.get('/admin/ban', isLoggedIn, moderation.quickBan);
	app.get('/admin/remove', isLoggedIn, moderation.quickRemove);
	app.get('/admin/menu', isLoggedIn, moderation.menuSearch);
	app.get('/admin/set', isLoggedIn, moderation.setAdmin);
	app.get('/admin/category', isLoggedIn, moderation.viewCategories);
	app.get('/admin/subcategory', isLoggedIn, moderation.viewSubCategories);

	app.post('/admin/category/add', isLoggedIn, moderation.addCategory);
	app.post('/admin/subcategory/add', isLoggedIn, moderation.addSubCategory);

	app.post('/admin/subcategory/remove', isLoggedIn, moderation.removeSubCategory);

	// LISTING

	var Listing = require('../config/listing.js');

	// LISTING

	app.get('/new-listing', isLoggedIn, Listing.newListing);
	app.get('/new-listing/service', isLoggedIn, Listing.serviceNew);
	app.get('/new-listing/request', isLoggedIn, Listing.requestNew);

	app.get('/listing/category', Listing.category);
	app.get('/listing/subcategories/:category', Listing.subCategory);

	app.get('/api/listing/:listingID', Listing.apiListing);
	app.get('/api/listing/user/info/:username', Listing.userInfo);

	app.post('/new-listing/service', isLoggedIn, Listing.createService);
	app.post('/new-listing/request', isLoggedIn, Listing.createRequest);

	app.get('/listing/:id/buy', isLoggedIn, Listing.purchase);
	app.get('/listing/:id/manage', isLoggedIn, Listing.edit);
	app.post('/listing/:id/manage', isLoggedIn, Listing.postEdit);
	app.get('/listing/:id/remove', isLoggedIn, Listing.removeListing);
	app.get('/s/:id/feedback', isLoggedIn, Listing.feedback);
	app.get('/api/s/:id/feedback', isLoggedIn, Listing.feedbackAPI);
	app.get('/s/:listingID/feedback/count', Listing.feedbackCount);

	app.get('/api/listing', Listing.servicesAPI);
	app.get('/api/requests', Listing.requestsAPI);

	// OFFERS

	var Offer = require('../config/offer.js');

	app.get('/listing/:listingID/offers', isLoggedIn, Offer.viewOffers);
	app.get('/api/listing/:listingID/offers', isLoggedIn, Offer.viewOffersAPI);

	app.get('/offer/accept/:id', isLoggedIn, Offer.acceptOffer);
	app.get('/offer/decline/:id', isLoggedIn, Offer.declineOffer);

	app.post('/offer/create/:listingID', isLoggedIn, Offer.makeOffer);

	// DEAL

	var Deal = require('../config/deal.js');

	app.get('/deal/:dealID', isLoggedIn, Deal.viewDeal);
	app.get('/deal/:dealID/feedback', isLoggedIn, Deal.feedback);
	app.post('/deal/:dealID/feedback', isLoggedIn, Deal.giveFeedback);
	app.get('/api/deal/:dealID', isLoggedIn, Deal.dealAPI);
	app.get('/api/deal/:dealID/messages', isLoggedIn, Deal.dealMessagesAPI);
	app.post('/deal/:dealID/new/message', isLoggedIn, Deal.dealMessageCreate);

	app.get('/deal/accept/:dealID', isLoggedIn, Deal.dealAccept);
	app.get('/deal/cancel/:dealID', isLoggedIn, Deal.dealCancel);

	// ESCROW

	app.get('/dispute/new/:dealID', isLoggedIn, Deal.newDispute);

	app.get('/listing/:listingID/watchlist', isLoggedIn, Listing.watchlistAdd);

	// ESCROW

	var Escrow = require('../config/escrow.js');

	app.get('/dispute/:disputeID', isLoggedIn, Escrow.dispute);
	app.get('/disputes', Escrow.viewDisputes);

	app.get('/api/dispute/:disputeID', isLoggedIn, Escrow.apiDispute);
	app.get('/api/dispute/:disputeID/messages', isLoggedIn, Escrow.apiMessageDispute);
	app.get('/api/dispute/:disputeID/votes', Escrow.apiVotes);
	app.get('/api/dispute/:disputeID/voted', Escrow.isVoted);
	app.get('/api/dispute/:disputeID/decide', Escrow.decide);
	app.get('/api/disputes', Escrow.apiDisputes);

	app.post('/dispute/message/:disputeID', isLoggedIn, Escrow.composeMessage);
	app.post('/dispute/vote/:disputeID', isLoggedIn, Escrow.voteDispute);

	// SEARCHES

	var Search = require('../config/search.js');

	app.get('/search', Search.search);
	app.get('/search/results', Search.result);

	app.get('/search/query', Search.query);

	/* 

	app.get('/categories')
	app.get('/categories/:subcategories')
	app.get('/deals')
	
	*/

	// HELP

	var Help = require('../config/help.js');

	app.get('/help', Help.index);

	app.get('/help/user/settings', Help.userSettings);
	app.get('/help/user/wallet', Help.userWallet);
	app.get('/help/user/account', Help.userAccount);
	app.get('/help/user/profile', Help.userProfile);

	app.get('/help/listing/start', Help.listingStart);
	app.get('/help/listing/edit', Help.listingEdit);
	app.get('/help/listing/remove', Help.listingRemove);
	app.get('/help/listing/fees', Help.listingFees);
	app.get('/help/listing/type', Help.listingType);
	app.get('/help/listing/report', Help.listingReport);
	app.get('/help/listing/management', Help.listingManagement);

	app.get('/help/deal/feedback', Help.dealFeedback);
	app.get('/help/deal/dispute', Help.dealDispute);
	app.get('/help/deal/offer', Help.dealOffer);
	app.get('/help/deal/deal', Help.dealDeal);
	app.get('/help/deal/options', Help.dealOptions);

	// CATCH ALL

	app.get('/u/:username', user.u);
	app.get('/r/:id', Listing.r);
	app.get('/s/:id', Listing.s);


	app.get('/*', general.error);
}

var mongoose = require('mongoose');
var User = require('../db/user');
var Listing = require('../db/listing');
var Category = require('../db/category');
var subCategory = require('../db/subcategory');
var location = require('../db/location');
var Offer = require('../db/offer');
var Transaction = require('../db/transaction');
var Feedback = require('../db/feedback');
var Deal = require('../db/deal');
var dealMessage = require('../db/dealmessage');
var Dispute = require('../db/dispute');
var Watchlist = require('../db/watchlist');
var sanitize = require('strip-js');
var lib = require('../config/lib.js');

exports.dealAccept = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID, 'completed': 0, 'dispute': 0, 'feedback': 0}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '{}' || dealResult == '[]' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(dealResult.customer == req.session.user.username) {

					// PAY WORKER

					var User = mongoose.model('User');
					User.findOne({'username': dealResult.worker}).exec(function(err, result) {
						if(err || result == '' || result == '[]' || result == '{}' || result == null) {
							return 0;
						} else {
							var newAmount = Number(result.taskcoin) + Number(dealResult.amount);
							User.update({'username': dealResult.worker}, {'taskcoin': newAmount}).exec();

							return 1;
						}
					});

					// DEDUCT FROM CUSTOMER HOLDING

					var User = mongoose.model('User');
					User.findOne({'username': dealResult.customer}).exec(function(err, result) {
						if(err || result == '' || result == '[]' || result == '{}' || result == null) {
							return 0;
						} else {
							var newHolding = Number(result.holding) - Number(dealResult.amount);
							User.update({'username': dealResult.customer}, {'holding': newHolding});

							return 1;
						}
					});

					// TRANSACTIONS

					var Transaction = mongoose.model('Transaction');
					var newTransaction = new Transaction();
					newTransaction.from = dealResult.customer;
					newTransaction.to = dealResult.worker;
					newTransaction.date = Date.now();
					newTransaction.amount = dealResult.amount;
					newTransaction.reason = "Deal (ID="+dealID+") successfully completed. ";
					newTransaction.save();

					newTransaction.from = dealResult.worker;
					newTransaction.to = dealResult.customer;
					newTransaction.date = Date.now();
					newTransaction.amount = 0 - dealResult.amount;
					newTransaction.reason = "Deal (ID="+dealID+") successfully completed. ";
					newTransaction.save();

					// UPDATE DEAL

					Deal.update({'_id': dealID}, {'completed': 1, 'feedback': 1}).exec();

					// REDIRECT TO DEAL AGAIN

					res.redirect('/deal/'+dealID);
				} else {
					res.redirect('/404');
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.dealCancel = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID, 'completed': 0, 'dispute': 0, 'feedback': 0}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(dealResult.worker == req.session.user.username) {
					var User = mongoose.model('User');
					User.findOne({'username': dealResult.customer}).exec(function(err, userResult) {
						if(err || userResult == '' || userResult == '{}' || userResult == '[]' || userResult == null) {
							res.redirect('/sign-in');
						} else {
							var newAmount = Number(userResult.taskcoin) + Number(dealResult.amount);
							var newHolding = Number(userResult.holding) - Number(dealResult.amount);

							User.update({'username': dealResult.customer}, {'taskcoin': newAmount, 'holding': newHolding}).exec();
						}
					});

					var Transaction = mongoose.model('Transaction');
					var newTransaction = new Transaction();
					newTransaction.from = '';
					newTransaction.to = dealResult.customer;
					newTransaction.date = Date.now();
					newTransaction.amount = dealResult.amount;
					newTransaction.reason = 'Deal cancelled. Holding transferred back into main wallet.';
					newTransaction.save();

					Deal.findOneAndRemove({'_id': dealID, 'worker': req.session.user.username}).exec();

					res.redirect('/user/wallet');
				} else {
					res.redirect('/404');
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.newDispute = function(req, res) {
	var dealID = sanitize(String(req.params.dealID));
	var reason = 'arse arse arse arse arse this is crap to fill up the min requirements';
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID, 'completed': 0}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(reason.length >= 30 && reason.length <= 10000) {
					if(dealResult.worker == req.session.user.username) {
						Deal.update({'_id': dealID}, {'completed': 1, 'feedback': 1, 'dispute': 1}).exec();

						var Dispute = mongoose.model('Dispute');
						var newDispute = new Dispute();
						newDispute.worker = dealResult.worker;
						newDispute.customer = dealResult.customer;
						newDispute.created = Date.now();
						newDispute.reason = reason;
						newDispute.dealID = dealID;
						newDispute.listingID = dealResult.listingID;
						newDispute.amount = dealResult.amount;
						newDispute.verdict = 0;

						newDispute.save(function(err, result) {
							if(err) {
								newDispute.save();
							} else {
								res.redirect('/dispute/'+result._id);
							}
						});
					} else if(dealResult.customer == req.session.user.username) {
						Deal.update({'_id': dealID}, {'completed': 1, 'feedback': 1, 'dispute': 1}).exec();

						var Dispute = mongoose.model('Dispute');
						var newDispute = new Dispute();
						newDispute.customer = dealResult.customer;
						newDispute.worker = dealResult.worker;
						newDispute.created = Date.now();
						newDispute.reason = reason;
						newDispute.dealID = dealID;
						newDispute.listingID = dealResult.listingID;
						newDispute.amount = dealResult.amount;
						newDispute.verdict = 0;

						newDispute.save(function(err, result) {
							if(err) {
								newDispute.save();
							} else {
								res.redirect('/dispute/'+result._id);
							}
						});
					} else {
						res.redirect('/404');
					}
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.viewDeal = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(dealResult.worker == req.session.user.username || dealResult.customer == req.session.user.username) {
					res.render('deal', {
						title: 'Deal | Taskly',
						dealWorker: String(dealResult.worker),
						dealCustomer: String(dealResult.customer),
						dealPrice: dealResult.amount,
						dealNote: dealResult.note,
						user: req.session.user,
						id: dealResult._id,
						websiteName: 'Taskly'
					});
				} else {
					res.redirect('/404');
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.dealAPI = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(dealResult.worker == req.session.user.username || dealResult.customer == req.session.user.username) {
					res.send(dealResult);
				} else {
					res.redirect('/404');
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.dealMessagesAPI = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(dealResult.dispute == 0) {
					if(dealResult.customer == req.session.user.username || dealResult.worker == req.session.user.username) {
						var dealMessage = mongoose.model('dealMessage');
						dealMessage.find({'dealID': dealID}).sort({'date': -1}).limit(25).exec(function(err, dealMessageResult) {
							if(err || dealMessageResult == '' || dealMessageResult == '{}' || dealMessageResult == '[]' || dealMessageResult == null) {
								res.send(404);
							} else {
								res.send(dealMessageResult);
							}
						});
					} else {
						res.redirect('/404');
					}
				} else {
					var dealMessage = mongoose.model('dealMessage');
					dealMessage.find({'dealID': dealID}).sort({'date': 1}).exec(function(err, dealMessageResult) {
						if(err || dealMessageResult == '' || dealMessageResult == '{}' || dealMessageResult == '[]' || dealMessageResult == null) {
							res.send(404);
						} else {
							res.send(dealMessageResult);
						}
					});
				}
			}
		});
	} else {
		res.redirect('/404');
	}
};

exports.dealMessageCreate = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();
	var message = String(req.body.message).trim();
	if(dealID && message) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID, 'completed': 0}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {
				if(dealResult.worker == req.session.user.username || dealResult.customer == req.session.user.username) {
					if(message.length >= 30 && message.length <= 10000) {
						var dealMessage = mongoose.model('dealMessage');
						var newMessage = new dealMessage();
						newMessage.username = req.session.user.username;
						newMessage.dealID = dealID;
						newMessage.date = Date.now();
						newMessage.message = message;
						newMessage.save();

						res.redirect('/deal/'+dealID);
					} else {
						res.redirect('/deal/'+dealID);
					}
				} else {
					res.redirect('/404');
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.feedback = function(req, res) {
	var dealID = sanitize(req.params.dealID);
	if(dealID) {
		var Deal = mongoose.model('Deal');
		Deal.findOne({'_id': dealID}).exec(function(err, dealResult) {
			if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
				res.redirect('/404');
			} else {	
				if(dealResult.customer == req.session.user.username) {
					res.render('feedback', {
						title: 'Feedback | Taskly',
						user: req.session.user,
						dealPrice: dealResult.amount,
						dealNote: dealResult.note,
						dealWorker: dealResult.worker,
						dealCustomer: dealResult.customer,
						reason: '',
						id: dealID,
						websiteName: 'Taskly'
					});
				} else {
					res.redirect('/404');
				}
			}
		});
	} else {
		res.redirect('/404');	
	}
}

exports.giveFeedback = function(req, res) {
	var dealID = sanitize(String(req.params.dealID)).trim();

	var rating = String(req.body.rating).trim();
	var reason = String(req.body.reason).trim();

	if(dealID) {
		if(reason && rating) {
			if(reason.length >= 10 && reason.length <= 150) {
				if(rating == '0' || rating == '1') {
					var Deal = mongoose.model('Deal');
					Deal.findOne({'_id': dealID, 'feedback': 1}).exec(function(err, dealResult) {
						if(err || dealResult == '' || dealResult == '[]' || dealResult == '{}' || dealResult == null) {
							res.redirect('/404');
						} else {	

							// CHECK IT"S CUSTOMER SENDING

							if(dealResult.customer == req.session.user.username) {
								var Feedback = mongoose.model('Feedback');
								Feedback.findOne({'customer': req.session.user.username, 'worker': dealResult.worker, 'listingID': dealResult.listingID, 'dealID': dealID}).exec(function(err, feedbackResult) {
									if(err || feedbackResult == '' || feedbackResult == '[]' || feedbackResult == '{}' || feedbackResult == null) {
										// CREATE NEW FEEDBACK

										var newFeedback = new Feedback();
										newFeedback.customer = req.session.user.username;
										newFeedback.worker = dealResult.worker;
										newFeedback.dealID = dealResult._id;
										newFeedback.listingID = dealResult.listingID;
										newFeedback.amount = dealResult.amount;
										newFeedback.rating = rating;
										newFeedback.reason = reason;
										newFeedback.date = Date.now();

										newFeedback.save();

										res.redirect('/u/'+dealResult.worker+'/feedback');
									} else {
										// FEEDBACK ALREADY GIVEN

										res.redirect('/u/'+dealResult.worker+'/feedback');
									}
								});
							} else {
								res.redirect('/404');
							}
						}
					});
				} else {
					res.send('Choose a rating');
				}
			} else {
				res.send('Reason has to be between 10 and 150 characters');
			}
		} else {
			res.send('Fill in all forms');
		}
	} else {
		res.redirect('/404');
	}
}

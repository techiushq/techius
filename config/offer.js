var mongoose = require('mongoose');
var User = require('../db/user');
var Listing = require('../db/listing');
var Category = require('../db/category');
var subCategory = require('../db/subcategory');
var location = require('../db/location');
var Offer = require('../db/offer');
var Transaction = require('../db/transaction');
var Deal = require('../db/deal');
var Dispute = require('../db/dispute');
var Watchlist = require('../db/watchlist');
var sanitize = require('strip-js');

exports.makeOffer = function(req, res) {
	var id = sanitize(String(req.params.listingID)).trim();
	var amount = sanitize(String(req.body.amount)).trim();
	var note = String(req.body.note).trim();

	if(id && amount) {
		if(id.length == 24) {
			var Listing = mongoose.model('Listing');
			Listing.findOne({'_id': id, 'public': 1}).exec(function(err, listingResult) {
				if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
					res.redirect('/404');
				} else {
					var Offer = mongoose.model('Offer');
					if(listingResult.username == req.session.user.username) {
						res.redirect('/404');
					} else {	
						if(listingResult.type == 0) {
							Offer.findOne({'listingID': id, 'worker': req.session.user.username}).exec(function(err, offerResult) {
								if(err || offerResult == '' || offerResult == '[]' || offerResult == '{}' || offerResult == null) {
									// REQUEST

									// THIS ARE "WORKERS" MAKING OFFER

									var newOffer = new Offer();
									newOffer.worker = req.session.user.username;
									newOffer.customer = listingResult.username;
									newOffer.created = Date.now();
									newOffer.type = 0;
									newOffer.listingID = id;
									newOffer.amount = Number(amount);
									newOffer.note = note;
									newOffer.accepted = 0;
									newOffer.save();

									res.redirect('/user/requests/offers');
								} else {
									res.render('purchase', {
										title: 'Purchase | Taskly',
										user: req.session.user,
										reason: 'Offer already made',
										listingID: id,
										listingName: listingResult.title,
										category: listingResult.category,
										subCategory: listingResult.subcategory,
										location: listingResult.location,
										timeLength: listingResult.done,
										price: listingResult.amount
									});
								}
							}); 
							


						} else if(listingResult.type == 1) {
							var User = mongoose.model('User');
							User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
								if(err || userResult == '' || userResult == '[]' || userResult == '{}' || userResult == null) {
									res.redirect('/sign-in');
								} else {
									if(Number(userResult.taskcoin) >= Number(amount)) {
										Offer.findOne({'listingID': id, 'customer': req.session.user.username}).exec(function(err, offerResult) {
											if(err || offerResult == '' || offerResult == '[]' || offerResult == '{}' || offerResult == null) {
												// SERVICE 

												// THIS IS THE "CUSTOMER" MAKING AN OFFER

												
												var newOffer = new Offer();
												newOffer.customer = req.session.user.username;
												newOffer.worker = listingResult.username;
												newOffer.created = Date.now();
												newOffer.type = 1;
												newOffer.listingID = id;
												newOffer.amount = Number(amount);
												newOffer.note = note;
												newOffer.accepted = 0;
												newOffer.save();

												User.findOne({'username': req.session.user.username}).exec(function(err, userResult) {
													var newAmount = Number(userResult.taskcoin) - Number(amount);
													var newHolding = Number(userResult.holding) + Number(amount);
													User.update({'username': req.session.user.username}, {'taskcoin': newAmount, 'holding': newHolding}).exec();
												});

												var Transaction = mongoose.model('Transaction');
												var newTransaction = new Transaction();
												newTransaction.from = '';
												newTransaction.to = req.session.user.username;
												newTransaction.created = Date.now();
												newTransaction.amount = Number(amount);
												newTransaction.reason = 'Moved from credit to holding';
												newTransaction.save();

												res.redirect('/user/requests/offers');
											} else {
												res.render('purchase', {
													title: 'Purchase | Taskly',
													user: req.session.user,
													reason: 'Offer already made.',
													listingID: id,
													listingName: listingResult.title,
													category: listingResult.category,
													subCategory: listingResult.subcategory,
													location: listingResult.location,
													timeLength: listingResult.done,
													price: listingResult.amount
												});
											}
										});
									} else {
										res.render('purchase', {
											title: 'Purchase | Taskly',
											user: req.session.user,
											reason: 'Not enough TaskCoin to proceed with deal',
											listingID: id,
											listingName: listingResult.title,
											category: listingResult.category,
											subCategory: listingResult.subcategory,
											location: listingResult.location,
											timeLength: listingResult.done,
											price: listingResult.amount
										});
									}
								}
							});
						} else {
							res.redirect('/404');
						}
					}
				}
			});
		}
	} else {
		res.redirect('/404');
	}
};

exports.acceptOffer = function(req, res) {
	var offerID = sanitize(req.params.id);
	if(offerID) {
		var Offer = mongoose.model('Offer');
		Offer.findOne({'_id': offerID, 'accepted': 0}).exec(function(err, offerResult) {
			var Listing = mongoose.model('Listing');
			Listing.findOne({'_id': offerResult.listingID, 'public': true}).exec(function(err, listingResult) {
				if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
					// LISTING NOT PUBLIC, CAN'T BE ACCEPTED!!

					res.redirect('/404');
				} else {
					console.log(offerResult);
					if(err || offerResult == '' || offerResult == '[]' || offerResult == '{}' || offerResult == null) {
						res.redirect('/404');
					} else {
						// REQUEST = 0, SERVICE = 1
						if(offerResult.type == true) {

							if(offerResult.worker == req.session.user.username) {

								var Deal = mongoose.model('Deal');
								var newDeal = new Deal();
								newDeal.worker = offerResult.worker;
								newDeal.customer = offerResult.customer;
								newDeal.listingID = offerResult.listingID;
								newDeal.amount = offerResult.amount;
								newDeal.note = offerResult.note;
								newDeal.dispute = 0;
								newDeal.completed = 0;
								newDeal.feedback = 0;

								Offer.count({'listingID': offerResult.listingID, 'to': req.session.user.username, 'accepted': 0}).exec(function(err, offerCount) {
									if(err || offerResult == '' || offerCount == '[]' || offerCount == '{}' || offerCount == null) {
										return 0;
									} else {
										for(var i = 0; i < offerCount; i++) {
											Offer.findOne({'listingID': offerResult.listing, 'customer': req.session.user.username, 'accepted': 0}).exec(function(err, findOneOfferResult) {
												if(err) {
													return 0;
												} else {
													Listing.update({'_id': offerResult.listingID, 'public': 1}, {'public': 0}).exec();

													Offer.findOneAndRemove({'listingID': offerResult.listing, 'customer': req.session.user.username, 'worker': findOneOfferResult.from, 'amount': findOneOfferResult.amount}).exec();
												}
											});
										};
									}

									Offer.findOneAndRemove({'_id': offerID}).exec();
								});

								newDeal.save(function(err, save) {
									if(err) {
										newDeal.save();
									} else {
										res.redirect('/deal/'+save._id);
									}
								});

							} else {
								res.redirect('/404');
							}
 
						} else if(offerResult.type == false) {	
							if(offerResult.customer == req.session.user.username) {
								
								// CREATE NEW DEAL

								var Deal = mongoose.model('Deal');
								var newDeal = new Deal();
								newDeal.worker = offerResult.worker;
								newDeal.customer = offerResult.customer;
								newDeal.listingID = offerResult.listingID;
								newDeal.amount = offerResult.amount;
								newDeal.note = offerResult.note;
								newDeal.dispute = 0;
								newDeal.completed = 0;
								newDeal.feedback = 0;
								newDeal.save(function(err, save) {
									if(err) {
										newDeal.save();
									} else {
										if(save) {
											res.redirect('/deal/'+save._id);
										} else {
											return 0;
										}
									}
								});

								Offer.findOneAndRemove({'listingID': offerResult.listingID}).exec();

								Listing.update({'_id': offerResult.listingID}, {'public': 0}).exec();

							} else {
								res.redirect('/404');
							}
						} else {
							res.redirect('/404');
						}
					}
				}
			});
		});
	} else {
		res.redirect('/404');
	}
}

exports.declineOffer = function(req, res) {
	var offerID = sanitize(req.params.id);
	if(offerID) {
		var Offer = mongoose.model('Offer');
		Offer.findOne({'_id': offerID}).exec(function(err, offerResult) {
			if(err || offerResult == '' || offerResult == '[]' || offerResult == '{}' || offerResult == null) {
				res.redirect('/404');
			} else {

				// ENSURE THAT BOTH THE 'WORKER' AND 'CUSTOMER' CAN REVOKE THIS, BUT ONLY THEM!

				if(offerResult.worker == req.session.user.username || offerResult.customer == req.session.user.username) {
					Offer.findOneAndRemove({'_id': offerID}).exec();
				} else {
					return 0;
				}

				if(offerResult.customer == req.session.user.username) {
					res.send(200);
				} else {
					res.redirect('/user/offers');
				}
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.viewOffersAPI = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': listingID, 'username': req.session.user.username}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.send(404);
			} else {
				var Offer = mongoose.model('Offer');
				Offer.find({'listingID': listingID}).exec(function(err, offerResult) {
					if(err || offerResult == '' || offerResult == '[]' || offerResult == '{}' || offerResult == null) {
						res.send(404);
					} else {
						res.send(offerResult);
					}
				});
			}
		});
	} else {
		res.redirect('/404');
	}
}

exports.viewOffers = function(req, res) {
	var listingID = sanitize(req.params.listingID);
	if(listingID) {
		var Listing = mongoose.model('Listing');
		Listing.findOne({'_id': listingID, 'username': req.session.user.username}).exec(function(err, listingResult) {
			if(err || listingResult == '' || listingResult == '[]' || listingResult == '{}' || listingResult == null) {
				res.redirect('/404');
			} else {
				var Offer = mongoose.model('Offer');
				Offer.find({'listingID': listingID}).exec(function(err, offerResult) {
					if(err || offerResult == '' || offerResult == '[]' || offerResult == '{}' || offerResult == null) {
						res.render('viewoffers', {
							title: 'View Offers | Taskly',
							user: req.session.user,
							listingID: listingID,
							listingTitle: listingResult.title,
							listingAmount: listingResult.amount,
							listingDescription: listingResult.description.split(0,100),
							listingCategory: listingResult.category,
							listingSubCategory: listingResult.subcategory,
							listingLocation: listingResult.location,
							listingUsername: listingResult.username,
							listingType: listingResult.type,
							listingDone: listingResult.done,
							offers: '',
							op: String(listingResult.username),
							websiteName: 'Taskly'
						});
					} else {
						res.render('viewoffers', {
							title: 'View Offers | Taskly',
							user: req.session.user,
							listingID: listingID,
							listingTitle: listingResult.title,
							listingAmount: listingResult.amount,
							listingDescription: listingResult.description.split(0,100),
							listingCategory: listingResult.category,
							listingSubCategory: listingResult.subcategory,
							listingLocation: listingResult.location,
							listingUsername: listingResult.username,
							listingType: listingResult.type,
							listingDone: listingResult.done,
							offers: JSON.stringify(offerResult),
							op: String(listingResult.username),
							websiteName: 'Taskly'
						});
					}
				});	
			}
		});
	} else {
		res.redirect('/404');
	}
}

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/*

type - ip, withdrawal, deposit, service, request

*/

var activityListing = mongoose.Schema({
	username: String,
	date: Date,
	type: String,
	value: String
});

module.exports = mongoose.model('userActivity', activityListing);

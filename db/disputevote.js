var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/*


*/

var disputeSchema = mongoose.Schema({
    username: String,
    disputeID: String,
    date: Date,
    side: String,
    amount: Number
});

module.exports = mongoose.model('disputeVote', disputeSchema);

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/*


*/

var disputeSchema = mongoose.Schema({
    username: String,
    disputeID: String,
    date: Date,
    message: String
});

module.exports = mongoose.model('disputeMessage', disputeSchema);

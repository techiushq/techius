var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');

/* 

	

*/

var messageSchema = mongoose.Schema({
	from: String,
	to: String,
	title: String,
	message: String,
	read: Boolean,
	date: Date
});


messageSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Message', messageSchema);

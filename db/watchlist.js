var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* 

	

*/

var watchlistSchema = mongoose.Schema({
	username: String,
	listingID: String,
	date: Date
});

module.exports = mongoose.model('Watchlist', watchlistSchema);

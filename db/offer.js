var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');

/*

*/

var offerSchema = mongoose.Schema({
	worker: String,
	customer: String,
	created: Date,
	type: Boolean,
	listingID: String,
	amount: Number,
	note: String,
	accepted: Boolean
});

offerSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Offer', offerSchema);

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/*


*/

var dealMessageSchema = mongoose.Schema({
    username: String,
    dealID: String,
    date: Date,
    message: String
});

module.exports = mongoose.model('dealMessage', dealMessageSchema);

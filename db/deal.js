var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');

// DISPUTE - IS A DISPUTE OPENED?
// COMPLETED - LOCK DEAL
// DEAL CAN GIVE FEEDBACK "TO" USER

var dealSchema = mongoose.Schema({
    worker: String,
    customer: String,
    listingID: String,
    amount: Number,
    note: String,
    dispute: Boolean,
    completed: Boolean,
    feedback: Boolean
});

dealSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Deal', dealSchema);

var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');

/* 

from - who is sending
to - who is receiving
listing - listing ID

*/

var feedbackSchema = mongoose.Schema({
	customer: String,
	worker: String,
	dealID: String,
	listingID: String,
	amount: Number,
	rating: Boolean,
	reason: String,
	date: Date
});

feedbackSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Feedback', feedbackSchema);

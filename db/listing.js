var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');

/*

username - username of creator
created - date listing was created
category - category listing is in
location - location where listing can be done
title - title of listing
description - description of listing
amount - how much taskcoin is being offered
type - what is the type of listing? request or service (REQUEST = CUSTOMER) (SERVICE = WORKER)
photo - photo of listing
done - estimated completion date
feedback - the average feedback score of this listing
edited - has the listing been edited?
public - is the listing public?
offline - how long has the listing been offline for? (auto delete after a week)

*/

var listingSchema = mongoose.Schema({
	username: String,
	created: Date,
	category: String,
	subcategory: String,
	location: String,
	title: String,
	description: String,
	amount: Number,
	type: Boolean,
	photo: String,
	done: String,
	feedback: Number,
	edited: Boolean,
	public: Boolean,
	offline: Date
});

listingSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Listing', listingSchema);

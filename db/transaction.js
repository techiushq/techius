var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var mongoosePaginate = require('mongoose-paginate');

var transactionSchema = mongoose.Schema({
	from: String,
	to: String,
	date: Date,
  	amount: Number,
  	reason: String
});

transactionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Transaction', transactionSchema);

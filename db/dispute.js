var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/*

usernameA - person sending
usernameB - person receiving
created - date transaction was created
reason - why was the transaction sent
typeID: what is the ID of the deal
stage - 0 sending, 1 verifying, 2, sent
verdict - 0 = deciding, 1 = WORKER, 2 = CUSTOMER

*/

var disputeSchema = mongoose.Schema({
	worker: String,
	customer: String,
	created: Date,
	reason: String,
	dealID: String,
	listingID: String,
	amount: Number,
	verdict: Number
});

module.exports = mongoose.model('Dispute', disputeSchema);
